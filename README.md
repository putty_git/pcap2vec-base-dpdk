# pcap2vec-base-dpdk

#### 介绍
基于dpdk的pcap文件解析程序

#### 软件架构
1. 项目任务名称：PCAP2VEC程序开发

2. 项目概要设计：

   1）项目背景：

   - 报文分析基本情况如下：

     60个文件，492M，基础协议：tcp udp icmp; 上层应用复杂多样：http  ， https,   ftp, dns... ...

   - 目前信大要求提供的信息：流级统计信息共计66个 ；

   综合上述两点，本次开发仅支持TCP/UDP， icmp暂不开发待信大方明确特征后进行；

   任务：输入样本数据pcap文件中提取66维特征，输出csv形式文件。

   2）项目概要设计与说明：

   

![workflow](https://images.gitee.com/uploads/images/2021/0803/220841_e26a9d7b_9537924.png "pcap2vec.png")
上图为程序workflow，说明如下：

​			i) 任务分发模块：创建线程，线程数目暂定10个线程；根据pcap文件名字，将任务分发给各个线程；

​			ii) workflow:

​				（1）读取pcap文件；

​				（2）解析获取的数据包码流；从目前数据样例来看，解析MAC-------解析IPV4------解析TCP/UDP;

​				（3）查找/建立五元组流表；

​				（4）计算特征值；根据相关性将66维特征值分成3组（见文末）；

​				（5）输出csv文件；基于简化开发工作的目的，每个线程输出一个csv文件；文件格式，特征维度顺序按照与信大方约定的接口方式组织；


#### 安装教程

1.  git clone git@gitee.com:putty_git/pcap2vec-base-dpdk.git 
   
2.  目前环境已经在249上，创建了容器（84836e5197d6）主程序：/home/pcap2vec/nidps；dpdk:/home/pcap2vec/dpdk-stable-18.02.2
    （大家创建自己的程序目录只要修改makefile文件中APP_DIR = /home/pcap2vec/nidps即可）

3.  请按照nidps下面的makefile文件添加自己的代码。

#### 协作说明

1.  各位务必保证在容器环境下创建自己主程序目录nidps编译通过，且可以正常运行后在提交代码。
2.  每天晚6点后，不要提交代码。
3.  有问题请及时联系，务必保证在8.12号之前完成任务。

####  研发任务安排：

- [x] 建立项目工程，以gitee方式托管代码；
- [x] 程序workflow流程开发、任务分发模块开发；
- [x] pcap文件读取模块开发；
- [x] 五元组流表开发；
- [x] 数据包解析模块开发；
- [x] 特征组1开发；
- [x] 特征组2开发；
- [x] 特征组3开发；
- [x] 输出模块开发；
- [x] 程序集成测试；
- [x] 程序部署，暂以docker容器形式部署；制作image文件；


#### 集成测试：
 
1. 60个数据样例，分成四组，每人15个文件；
2. 要求：1.15个文件跑完程序没有运行异常；2.从15个文件里面选5个文件，5个文件每个文件选择一个小流，验证输出数据准确性；

#### 安装使用方法：

   1）本项目提供docker image文件，下载地址：链接：https://pan.baidu.com/s/1SS_Fq2QXYzPD8_E6O8yInw 
                                    提取码：rzoq

   2）装载镜像文件，启动容器，设置环境变量。

      i) 装载镜像：
```
docker load < pcap2vec-1.0.tar
```
      ii) 启动容器:
```
docker run -it --privileged -v /sys/bus/pci/drivers:/sys/bus/pci/drivers -v /sys/kernel/mm/hugepages:/sys/kernel/mm/hugepages -v /sys/devices/system/node:/sys/devices/system/node -v /dev:/dev pcap2vec:1.1
```
      iii) 设置环境变量
     
```
      export RTE_SDK=/home/dpdk/dpdk-stable-18.02.2
      export RTE_TARGET=x86_64-native-linuxapp-gcc
      export EXTRA_CFLAGS='-O0 -g'

```

   3）执行命令启动数据包分析解析功能

```
      cd /home/pcap2vec/nidps
      ./build/nidps -c 0x1 -n 4 -- -i /home/pcap2vec/nidps/pcaps/ -o ./rst.csv
```

      -c 指定线程 16进制那一位置1表示那一位cpu启用；
      -n 内存通道不管；
      -i  输入pcap文件的所在目录，以'/'结尾；
      -o   结果输出csv文件


#### 异常处理：
      1. 针对pcap数据包长度异常处理如下：
        1）int ParseL2ToL4LayerProtoInfo(const uint8_t *pkt, const struct pcap_pkthdr *pkthdr, FIVE_TUPLE_KEY *tuple_info, PKT_STATS_INFO *pkt_stats_info)
        解析接口增加一个入参：本次实际buffer长度reallen，即
       从 struct pcap_pkthdr *pkthdr 结构体中读取的长度超过reallen，则判断为当前报文被截断，需要从mac协议解析，修正一些协议字段；
       如果reallen== 读取的长度，认为报文没有截断； mbuf 增加保护超过1500长度截断；
        2）特征值计算时除了pktlen计算使用pktlen以外，其余协议长度计算包括（mac\ip\tcp\udp头部 segment）不要使用pktlen，使用具体的协议字段；
        3）尽快完成自查自改；

#### 项目更改记录
    1. ===8.4==== ：
        - 增加数据包解析；
        - 增加tcp流表建立、查询、正常4次拆链删除；
        - 增加报文方向信息；
        - 增加特征组二；
    2. ===8.5==== : 
        - 增加流表老化机制；
        - 修复hash table bug;
        - 修改dpdk18.02版本hash table 增加计算表中有效记录函数；
        - 整合特征组2；
        - 增加整合输出模块；
        - 合入特征组1，未整合；
    3. ===8.6==== ：
        - 整合特征组1；
        - 合入特征组3，整合特征组3
        - 集成测试：修正mbuf下标错误越界问题；修正数据包异常导致程序异常问题；修正时间戳精度问题；
    4. ===8.7===== : 
        - 集成测试：
        - 修改多线程分别写文件导致Program terminated with signal 11, Segmentation fault问题---->修改为多线程写同一个文件；
        - 修改特征组1，流上单包统计问题；
    5. ===8.8==== ：
        - 打包docker镜像;
    6. ===8.9==== ：
        - 根据需求方提出需要在结果文件中增加最后一列标注每个流所属pcap文件

