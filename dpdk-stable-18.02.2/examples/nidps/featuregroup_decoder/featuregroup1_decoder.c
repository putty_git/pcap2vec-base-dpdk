#include "featuregroup1_decoder.h"

static void flow_duration(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	double   start_time  ;
    if (flow_tb->fwd_start_time < FLOAT_EPS)
    {
        start_time = flow_tb->bwd_start_time;
    }
    else if (flow_tb->bwd_start_time < FLOAT_EPS)
    {
        start_time = flow_tb->fwd_start_time;
    }
    else
    {
        start_time = (flow_tb->fwd_start_time < flow_tb->bwd_start_time)? flow_tb->fwd_start_time:flow_tb->bwd_start_time;
    }
	double finish_time   = (flow_tb->fwd_finish_time > flow_tb->bwd_finish_time)?flow_tb->fwd_finish_time:flow_tb->bwd_finish_time;
	if(finish_time > start_time)
	{
		flow_tb->flow_duration_time = finish_time - start_time;	
	}
}

static void total_fwd_pkts(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if (pkt->pkt_dir == DIR_FWD)
	{
		flow_tb->total_fwd_pkt++;
			
	}
		
}
static void total_bwd_pkts(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if (pkt->pkt_dir == DIR_BWD)
	{
		flow_tb->total_bwd_pkt++;
	}
}

static void total_fwd_bytes(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if(pkt->pkt_dir == DIR_FWD )
	{
		flow_tb->total_fwd_bytes += pkt->pkt_len;
	}
}

static void total_bwd_bytes(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if(pkt->pkt_dir == DIR_BWD)
	{
		flow_tb->total_bwd_bytes += pkt->pkt_len;
	}
}

static void fwd_pkt_len_min(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if(pkt->pkt_dir == DIR_FWD)
	{

		if((flow_tb->min_fwd_bytes == 0) || (pkt->pkt_len < flow_tb->min_fwd_bytes))
		{
			flow_tb->min_fwd_bytes = pkt->pkt_len;
		}
	}	
}

static void fwd_pkt_len_max(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if(pkt->pkt_dir == DIR_FWD)
	{
		if(pkt->pkt_len > flow_tb->max_fwd_bytes)
		{
			flow_tb->max_fwd_bytes = pkt->pkt_len;
		}
	}
}

static void fwd_pkt_len_mean(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if((pkt->pkt_dir == DIR_FWD) && (flow_tb->total_fwd_pkt >= 1))
	{
		flow_tb->mean_fwd_bytes = nidps_mean(flow_tb->mean_fwd_bytes, flow_tb->total_fwd_pkt-1, pkt->pkt_len);
	}
}

static void fwd_pkt_len_std(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if((pkt->pkt_dir == DIR_FWD) && (flow_tb->total_fwd_pkt >= 1))
	{
		flow_tb->std_fwd_bytes = nidps_std(flow_tb->std_fwd_bytes, flow_tb->mean_fwd_bytes, flow_tb->total_fwd_pkt-1, pkt->pkt_len);
	}
	
}


static void bwd_pkt_len_min(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if(pkt->pkt_dir == DIR_BWD)
	{
		if((flow_tb->min_bwd_bytes == 0) || (pkt->pkt_len < flow_tb->min_bwd_bytes))
		{
			flow_tb->min_bwd_bytes = pkt->pkt_len;
		}
	}
}

static void bwd_pkt_len_max(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if(pkt->pkt_dir == DIR_BWD)
	{
		if(pkt->pkt_len > flow_tb->max_bwd_bytes)
		{
			flow_tb->max_bwd_bytes = pkt->pkt_len;
		}
	}
}

static void bwd_pkt_len_mean(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if((pkt->pkt_dir == DIR_BWD) && (flow_tb->total_bwd_pkt >= 1))
	{
		flow_tb->mean_bwd_bytes = nidps_mean(flow_tb->mean_bwd_bytes, flow_tb->total_bwd_pkt-1, pkt->pkt_len);
	}
	
}


static void bwd_pkt_len_std(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if((pkt->pkt_dir == DIR_BWD) && (flow_tb->total_bwd_pkt >= 1))
	{
		flow_tb->std_bwd_bytes = nidps_std(flow_tb->std_bwd_bytes, flow_tb->mean_bwd_bytes, flow_tb->total_bwd_pkt-1, pkt->pkt_len);
	}
	
}

static void flow_bytes_second(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	int total_bytes     = flow_tb->total_fwd_bytes + flow_tb->total_bwd_bytes;
    double start_time  ;
    double total_time;
    if (flow_tb->fwd_start_time < FLOAT_EPS)
    {
        start_time = flow_tb->bwd_start_time;
    }
    else if (flow_tb->bwd_start_time < FLOAT_EPS)
    {
        start_time = flow_tb->fwd_start_time;
    }
    else
    {
        start_time = (flow_tb->fwd_start_time < flow_tb->bwd_start_time)? flow_tb->fwd_start_time:flow_tb->bwd_start_time;
    }
    
	double finish_time   = (flow_tb->fwd_finish_time > flow_tb->bwd_finish_time)?flow_tb->fwd_finish_time:flow_tb->bwd_finish_time;
	if (finish_time > start_time)
	{
		total_time = finish_time - start_time;
	}
	if (total_time < FLOAT_EPS)
	{
		return;
	}
    flow_tb->flow_bytes_second = total_bytes/total_time;
	
}

static void flow_pkts_second(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	double start_time;      
	double total_time; 
    
    if (flow_tb->fwd_start_time < FLOAT_EPS)
    {
        start_time = flow_tb->bwd_start_time;
    }
    else if (flow_tb->bwd_start_time < FLOAT_EPS)
    {
        start_time = flow_tb->fwd_start_time;
    }
    else
    {
        start_time = (flow_tb->fwd_start_time < flow_tb->bwd_start_time)? flow_tb->fwd_start_time:flow_tb->bwd_start_time;
    }
    
	double finish_time   = (flow_tb->fwd_finish_time > flow_tb->bwd_finish_time)?flow_tb->fwd_finish_time:flow_tb->bwd_finish_time;
	int total_pkts = flow_tb->total_fwd_pkt + flow_tb->total_bwd_pkt;
    
    if (finish_time > start_time)
	{
		total_time = finish_time - start_time;
	}
	if (total_time < FLOAT_EPS)
	{
		return;
	}
	flow_tb->flow_pkts_second = total_pkts/total_time ;
}

static void fwd_pkts_second(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	double fwd_time;
    if (flow_tb->fwd_start_time < FLOAT_EPS)
    {
        fwd_time = 0;
    }
    else if(flow_tb->fwd_finish_time > flow_tb->fwd_start_time)
    {
        fwd_time= flow_tb->fwd_finish_time - flow_tb->fwd_start_time;
    }
	if (fwd_time < FLOAT_EPS)
	{
		return;
	}
	flow_tb->fwd_pkts_second = flow_tb->total_fwd_pkt/fwd_time;
}



static void bwd_pkts_second(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{ 
	double bwd_time;
    if (flow_tb->bwd_start_time < FLOAT_EPS)
    {
        bwd_time = 0;
    }
    else if(flow_tb->bwd_finish_time > flow_tb->bwd_start_time)
    {
        bwd_time= flow_tb->bwd_finish_time - flow_tb->bwd_start_time;
    }

	if (bwd_time < FLOAT_EPS)
	{
		return;
	}

	flow_tb->bwd_pkts_second = flow_tb->total_bwd_pkt/bwd_time;
	
}


static void min_pkts_len(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{	
	if (flow_tb->min_fwd_bytes == 0)
	{
		flow_tb->min_pkts_len = flow_tb->min_bwd_bytes;
	}
	else if (flow_tb->min_bwd_bytes == 0)
	{
		flow_tb->min_pkts_len = flow_tb->min_fwd_bytes;
	}
	else
	{
 		flow_tb->min_pkts_len = nidps_min(flow_tb->min_fwd_bytes, flow_tb->min_bwd_bytes);
	}
		
}


static void max_pkts_len(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{

	flow_tb->max_pkts_len = nidps_max(flow_tb->max_fwd_bytes, flow_tb->max_bwd_bytes);


}

static void mean_pkts_len(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
   if(0 == (flow_tb->total_fwd_pkt + flow_tb->total_bwd_pkt))
   {
   		return;
   }
   flow_tb->mean_pkts_len =  (double)(flow_tb->total_fwd_bytes + flow_tb->total_bwd_bytes)/(flow_tb->total_fwd_pkt + flow_tb->total_bwd_pkt);
}

static void std_pkts_len(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if ( (flow_tb->total_fwd_pkt + flow_tb->total_bwd_pkt) < 1)
	{
		return;
	}
    int old_num = flow_tb->total_fwd_pkt + flow_tb->total_bwd_pkt -1;
	flow_tb->std_pkts_len = nidps_std(flow_tb->std_pkts_len, flow_tb->mean_pkts_len, old_num, pkt->pkt_len);
}

static void variance_pkts_len(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if ( (flow_tb->total_fwd_pkt + flow_tb->total_bwd_pkt) < 1)
	{
		return;
	}
    int old_num = flow_tb->total_fwd_pkt + flow_tb->total_bwd_pkt -1;
	flow_tb->variance_pkts_len = nidps_variance(flow_tb->std_pkts_len, flow_tb->mean_pkts_len, old_num, pkt->pkt_len);
}


static void down_up_ratio(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)   //报文数比
{
	if (0 == flow_tb->total_fwd_pkt)
	{
		return;
	}
	flow_tb->down_up_ratio = ((double)flow_tb->total_bwd_pkt) / flow_tb->total_fwd_pkt;
}

static void average_packet_size(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	int total_num = flow_tb->total_fwd_pkt + flow_tb->total_bwd_pkt ;
	if (0 == total_num)
	{
		return;
	}
	int tcp_len = 0;
	if(pkt->ip_len > pkt->pkt_ip_hdr_len)
	{
		tcp_len = pkt->ip_len - pkt->pkt_ip_hdr_len;
	}

	flow_tb->average_packet_size = ((flow_tb->average_packet_size * (total_num -1)) + tcp_len)/total_num;
}

void featuregroup1_init(PKT_STATS_INFO * pkt, LINK_STATS_INFO *flow_tb)
{
    if(DIR_FWD == pkt->pkt_dir )
    {
        if (0 == flow_tb->total_fwd_pkt)
        {
            flow_tb->fwd_start_time = pkt->pkt_timestamp;
        }
        flow_tb->fwd_finish_time = pkt->pkt_timestamp;
    }
    if (DIR_BWD == pkt->pkt_dir )
    {
        if (0 == flow_tb->total_bwd_pkt)
        {
            flow_tb->bwd_start_time = pkt->pkt_timestamp;
        }
        
        flow_tb->bwd_finish_time = pkt->pkt_timestamp;
    }
    
}

void featuregroup1_decode(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
    
    featuregroup1_init(pkt, flow_tb);
	flow_duration(pkt, flow_tb);
	total_fwd_pkts(pkt, flow_tb);
	total_bwd_pkts(pkt, flow_tb);
	total_fwd_bytes(pkt, flow_tb);
	total_bwd_bytes(pkt, flow_tb);
	fwd_pkt_len_min(pkt, flow_tb);
	fwd_pkt_len_max(pkt, flow_tb);
	fwd_pkt_len_std(pkt, flow_tb);
	fwd_pkt_len_mean(pkt, flow_tb);
	bwd_pkt_len_min(pkt, flow_tb);
	bwd_pkt_len_max(pkt, flow_tb);
	bwd_pkt_len_std(pkt, flow_tb);
	bwd_pkt_len_mean(pkt, flow_tb);
	flow_bytes_second(pkt, flow_tb);
	flow_pkts_second(pkt, flow_tb);
	fwd_pkts_second(pkt, flow_tb);
	bwd_pkts_second(pkt, flow_tb);
	min_pkts_len(pkt, flow_tb);
	max_pkts_len(pkt, flow_tb);
	variance_pkts_len(pkt, flow_tb);
	std_pkts_len(pkt, flow_tb);
	mean_pkts_len(pkt, flow_tb);
	down_up_ratio(pkt, flow_tb);
	average_packet_size(pkt, flow_tb);
	
	return;
}


