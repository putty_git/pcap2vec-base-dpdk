#ifndef __FEATUREGROUP_3_DECODER_H__
#define __FEATUREGROUP_3_DECODER_H__


#include "../common/include/common.h"

#include "../common/include/protocol_common.h"




void featuregroup3_decode(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_session);



#endif
