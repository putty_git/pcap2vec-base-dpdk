#ifndef __FEATUREGROUP_1_DECODER_H__
#define __FEATUREGROUP_1_DECODER_H__


#include "common.h"

#include "protocol_common.h"


void featuregroup1_decode(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb);



#endif



