
#include "featuregroup3_decoder.h"


#define NO_NEED_PROC            2
#define NEED_IDLE_PROC 			1
#define NEED_ACTIVE_PROC 		0

#define NEED_FWD_WIN_PROC 		1
#define NEED_BWD_WIN_PROC 		0


static int  winbytes_judge(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_session)
{
	if ((pkt->pkt_dir == DIR_FWD) && (pkt->pkt_ipproto_id == 6))
	{
		if (flow_session->fwd_tcp_first_flg == 0)
		{
			flow_session->fwd_tcp_first_flg = 1;

			flow_session->fwd_tcp_rx_first_window = pkt->tcp_rx_window;
		}
		else if (pkt->tcp_rx_window != flow_session->fwd_tcp_rx_first_window)
		{
			return NO_NEED_PROC;
		}
		
		return NEED_FWD_WIN_PROC;
	}
	else if ((pkt->pkt_dir == DIR_BWD) && (pkt->pkt_ipproto_id == 6))
	{
		if (flow_session->bwd_tcp_first_flg == 0)
		{
			flow_session->bwd_tcp_first_flg = 1;

			flow_session->bwd_tcp_rx_first_window = pkt->tcp_rx_window;
		}
		else if (pkt->tcp_rx_window != flow_session->bwd_tcp_rx_first_window)
		{
			return NO_NEED_PROC;
		}
		
		return NEED_BWD_WIN_PROC;
	}

	return NO_NEED_PROC;
}

static void fwd_init_winbytes_cal(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_session)
{
	flow_session->fwd_init_winbytes += pkt->pkt_len;

	return;
}

static void bwd_init_winbytes_cal(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_session)
{
	flow_session->bwd_init_winbytes += pkt->pkt_len;

	return;	
}

static void fwd_act_data_pkts_cal(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_session)
{
	if (pkt->pkt_dir == DIR_FWD)
	{
		uint32_t pkt_payload = pkt->ip_len - pkt->pkt_ip_hdr_len - pkt->pkt_trans_hdr_len;
		
		if (pkt_payload > 0)
		{
			flow_session->fwd_act_data_pkts++;
		}
	}	
}

static void fwd_seg_size_min(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_session)
{
	if (pkt->pkt_dir == DIR_FWD && pkt->pkt_ipproto_id == 6)
	{
		uint32_t total_trans_len = pkt->ip_len - pkt->pkt_ip_hdr_len;

		if (flow_session->fwd_seg_size_min == 0)
		{
			flow_session->fwd_seg_size_min = total_trans_len;
		}
		else
		{
			flow_session->fwd_seg_size_min = nidps_min(flow_session->fwd_seg_size_min, total_trans_len);
		}
	}	

	return;
}


static void active_min(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_session)
{
	double cur_iat = pkt->pkt_timestamp - flow_session->active_iat_timestamp_start;
	
	if (flow_session->active_min == 0)
	{
		flow_session->active_min = cur_iat;
	}
	else
	{
		flow_session->active_min = nidps_min(flow_session->active_min, cur_iat);
	}

	return;
}

static void active_mean(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_session)
{
	double cur_iat = pkt->pkt_timestamp - flow_session->active_iat_timestamp_start;
	
	flow_session->active_mean = nidps_mean(flow_session->active_mean, flow_session->active_iat_count - 1, cur_iat);

	return;
}

static void active_max(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_session)
{
	double cur_iat = pkt->pkt_timestamp - flow_session->active_iat_timestamp_start;
	
	flow_session->active_max = nidps_max(flow_session->active_max, cur_iat);

	return;
}

static void active_std(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_session)
{
	double cur_iat = pkt->pkt_timestamp - flow_session->active_iat_timestamp_start;
	
	flow_session->active_std = nidps_std(flow_session->active_std, flow_session->active_mean, flow_session->active_iat_count - 1, cur_iat);

	return;
}


static void idle_min(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_session)
{
  
	double cur_iat = pkt->pkt_timestamp - flow_session->idle_iat_timestamp_start;
	if (fabs(cur_iat - 0) <= FLOAT_EPS )
	{
	    return;
	}
	  
	if (flow_session->idle_min == 0)
	{
		flow_session->idle_min = cur_iat;
	}
	else
	{
		flow_session->idle_min = nidps_min(flow_session->idle_min, cur_iat);
	}
	
	return;
}

static void idle_mean(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_session)
{
	double cur_iat = pkt->pkt_timestamp - flow_session->idle_iat_timestamp_start;
	if (fabs(cur_iat - 0) <= FLOAT_EPS )
    {
   		return;
    }
	
	flow_session->idle_mean = nidps_mean(flow_session->idle_mean, flow_session->idle_iat_count - 1, cur_iat);
	return;
}

static void idle_max(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_session)
{
	double cur_iat = pkt->pkt_timestamp - flow_session->idle_iat_timestamp_start;
	if (fabs(cur_iat - 0) <= FLOAT_EPS )
	{
		return;
	}
	flow_session->idle_max = nidps_max(flow_session->idle_max, cur_iat);

	return;
}

static void idle_std(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_session)
{
	double cur_iat = pkt->pkt_timestamp - flow_session->idle_iat_timestamp_start;
	if (fabs(cur_iat - 0) <= FLOAT_EPS )
	{
		return;
	}
	
	flow_session->idle_std = nidps_std(flow_session->idle_std, flow_session->idle_mean, flow_session->idle_iat_count - 1, cur_iat);

	return;
}


static int active_judge(int res, PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_session)
{
	if (pkt->pkt_ipproto_id != 6 || flow_session->active_no_need_cal_flg)
	{
		return NO_NEED_PROC;
	}

	if (flow_session->active_first_flg == 0)
	{	
		flow_session->active_first_flg = 1;
		flow_session->active_iat_timestamp_start = pkt->pkt_timestamp; 
	}

	if (res == NEED_IDLE_PROC || flow_session->fin_or_rst_flg == 1)
	{
		if (flow_session->fin_or_rst_flg == 1)
		{
			flow_session->active_iat_timestamp_end = pkt->pkt_timestamp;
			flow_session->active_no_need_cal_flg = 1;
		}
		else
		{
			flow_session->active_iat_timestamp_end = flow_session->active_iat_timestamp_start;
		}
		
		flow_session->active_iat_count++;
		pkt->pkt_timestamp = flow_session->active_iat_timestamp_end;
		/** fix order */
		active_min(pkt, flow_session);
		active_max(pkt, flow_session);
		active_std(pkt, flow_session);
		active_mean(pkt, flow_session);	

		flow_session->active_iat_timestamp_start =flow_session->active_iat_timestamp_end;
	}


	return NO_NEED_PROC;
	

}


static int idle_judge(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_session)
{
	if (pkt->tcp_flags == TCP_SYN_FLAG)
	{
		flow_session->syn_flg = 1;
		return NO_NEED_PROC;	
	}
	if (pkt->tcp_flags == (TCP_SYN_FLAG|TCP_ACK_FLAG))
	{
		flow_session->synack_flg = 1;
		return NO_NEED_PROC;	
	}
	
	if (flow_session->syn_flg && flow_session->synack_flg)
	{
		flow_session->syn_flg = 0;
		flow_session->synack_flg = 0;
		
		return NO_NEED_PROC;	
	}
	
	if ((pkt->tcp_flags & TCP_FIN_FLAG) || (pkt->tcp_flags & TCP_RST_FLAG) )
	{
		flow_session->fin_or_rst_flg = 1;
		return NO_NEED_PROC;	
	}

	if (flow_session->fin_or_rst_flg == 1)
	{
		return NO_NEED_PROC;
	}

	if (pkt->pkt_ipproto_id != 6)
	{
		return NO_NEED_PROC;
	}
	
	uint32_t tcp_payload = pkt->ip_len - pkt->pkt_ip_hdr_len - pkt->pkt_trans_hdr_len;
	if (tcp_payload == 0)
	{
		if (flow_session->lastpkt_payload_zeor_flag == 1)
		{
			flow_session->last_idle_iat_duration = 1;
			flow_session->idle_iat_timestamp_end = pkt->pkt_timestamp;
		
			return NO_NEED_PROC;
		}
		else
		{
			flow_session->idle_iat_timestamp_start = pkt->pkt_timestamp;
			flow_session->lastpkt_payload_zeor_flag = 1;

			return NO_NEED_PROC;
		}
	}
	
	flow_session->lastpkt_payload_zeor_flag = 0;
	if (flow_session->last_idle_iat_duration != 0)
	{
		flow_session->last_idle_iat_duration = 0;
   		flow_session->idle_iat_count++;
    	pkt->pkt_timestamp = flow_session->idle_iat_timestamp_end;
		return NEED_IDLE_PROC;
		
	}
	
	return NO_NEED_PROC;
}


void featuregroup3_decode(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_session)
{
	int res = winbytes_judge(pkt, flow_session);
	
	if (NEED_FWD_WIN_PROC == res)
	{
		fwd_init_winbytes_cal(pkt, flow_session);
	}
	else if(NEED_BWD_WIN_PROC == res)
	{
		bwd_init_winbytes_cal(pkt, flow_session);
	}
	
	fwd_act_data_pkts_cal(pkt, flow_session);
	fwd_seg_size_min(pkt, flow_session);

	double tmp = pkt->pkt_timestamp;
    res = idle_judge(pkt, flow_session);
	if (res == NEED_IDLE_PROC)
	{
		/** fix order */
		idle_min(pkt, flow_session);
		idle_max(pkt, flow_session);
		idle_std(pkt, flow_session);
		idle_mean(pkt, flow_session);
	} 
	pkt->pkt_timestamp = tmp;

	
	tmp = pkt->pkt_timestamp;
	active_judge(res, pkt, flow_session);
	pkt->pkt_timestamp = tmp;
	
	return;
}



#if 0
#define UNITTESTS
#endif

#ifdef UNITTESTS

#include <stdio.h>

#include <string.h>



int test()
{
	PKT_STATS_INFO pkt;
	LINK_STATS_INFO flow_tb;
	memset(&pkt, 0 , sizeof(PKT_STATS_INFO));
	memset(&flow_tb, 0 , sizeof(LINK_STATS_INFO));

	pkt.pkt_dir = DIR_FWD;
	pkt.pkt_len = 60;
	pkt.ip_len = 52;
	pkt.pkt_mac_hdr_len = 8;
	pkt.pkt_ip_hdr_len = 20;
	pkt.pkt_trans_hdr_len = 20;
	pkt.tcp_flags = TCP_ACK_FLAG;
	pkt.pkt_timestamp = 1.1;
	pkt.pkt_ipproto_id = 6;
	pkt.tcp_rx_window = 1000;

	featuregroup3_decode(&pkt, &flow_tb);
	
	if (flow_tb.idle_min != 0
		|| flow_tb.idle_max != 0
		|| flow_tb.idle_mean != 0
		|| flow_tb.idle_std != 0
		|| flow_tb.active_min != 0
		|| flow_tb.active_max != 0
		|| flow_tb.active_mean != 0
		|| flow_tb.active_std != 0
		|| flow_tb.fwd_act_data_pkts != 1
		|| flow_tb.fwd_seg_size_min != 32
		|| flow_tb.fwd_init_winbytes != 60
		|| flow_tb.bwd_init_winbytes != 0)
	{
		printf("pkt1 test fail\n");
		return -1;
	}


	pkt.pkt_dir = DIR_BWD;
	pkt.pkt_len = 60;
	pkt.ip_len = 52;
	pkt.pkt_mac_hdr_len = 8;
	pkt.pkt_ip_hdr_len = 20;
	pkt.pkt_trans_hdr_len = 20;
	pkt.tcp_flags = TCP_ACK_FLAG;
	pkt.pkt_timestamp = 1.2;
	pkt.pkt_ipproto_id = 6;
	pkt.tcp_rx_window = 1000;
	
	featuregroup3_decode(&pkt, &flow_tb);
	
	if (flow_tb.idle_min != 0
		|| flow_tb.idle_max != 0
		|| flow_tb.idle_mean != 0
		|| flow_tb.idle_std != 0
		|| !(fabs(flow_tb.active_min - 0.1) <= FLOAT_EPS)
		|| !(fabs(flow_tb.active_max - 0.1) <= FLOAT_EPS)
		|| !(fabs(flow_tb.active_mean - 0.1) <= FLOAT_EPS)
		|| flow_tb.active_std != 0
		|| flow_tb.fwd_act_data_pkts != 1
		|| flow_tb.fwd_seg_size_min != 32
		|| flow_tb.fwd_init_winbytes != 60
		|| flow_tb.bwd_init_winbytes != 60)
	{
		printf("pkt2 test fail\n");
		return -1;
	}


	pkt.pkt_dir = DIR_FWD;
	pkt.pkt_len = 48;
	pkt.ip_len = 40;
	pkt.pkt_mac_hdr_len = 8;
	pkt.pkt_ip_hdr_len = 20;
	pkt.pkt_trans_hdr_len = 20;
	pkt.tcp_flags = TCP_ACK_FLAG;
	pkt.pkt_timestamp = 1.3;
	pkt.pkt_ipproto_id = 6;
	pkt.tcp_rx_window = 1000;
	
	featuregroup3_decode(&pkt, &flow_tb);
	
	if (flow_tb.idle_min != 0
		|| flow_tb.idle_max != 0
		|| flow_tb.idle_mean != 0
		|| flow_tb.idle_std != 0
		|| !(fabs(flow_tb.active_min - 0.1) <= FLOAT_EPS)
		|| !(fabs(flow_tb.active_max - 0.1) <= FLOAT_EPS)
		|| !(fabs(flow_tb.active_mean - 0.1) <= FLOAT_EPS)
		|| flow_tb.active_std != 0
		|| flow_tb.fwd_act_data_pkts != 1
		|| flow_tb.fwd_seg_size_min != 20
		|| flow_tb.fwd_init_winbytes != 108
		|| flow_tb.bwd_init_winbytes != 60)
	{
		printf("pkt3 test fail\n");
		return -1;
	}


	pkt.pkt_dir = DIR_BWD;
	pkt.pkt_len = 48;
	pkt.ip_len = 40;
	pkt.pkt_mac_hdr_len = 8;
	pkt.pkt_ip_hdr_len = 20;
	pkt.pkt_trans_hdr_len = 20;
	pkt.tcp_flags = TCP_ACK_FLAG;
	pkt.pkt_timestamp = 1.6;
	pkt.pkt_ipproto_id = 6;
	pkt.tcp_rx_window = 800;
	
	featuregroup3_decode(&pkt, &flow_tb);
	
	if (!(fabs(flow_tb.idle_min - 0.3) <= FLOAT_EPS)
		|| !(fabs(flow_tb.idle_max - 0.3) <= FLOAT_EPS)
		|| !(fabs(flow_tb.idle_mean - 0.3) <= FLOAT_EPS)
		|| flow_tb.idle_std != 0
		|| !(fabs(flow_tb.active_min - 0.1) <= FLOAT_EPS)
		|| !(fabs(flow_tb.active_max - 0.1) <= FLOAT_EPS)
		|| !(fabs(flow_tb.active_mean - 0.1) <= FLOAT_EPS)
		|| flow_tb.active_std != 0
		|| flow_tb.fwd_act_data_pkts != 1
		|| flow_tb.fwd_seg_size_min != 20
		|| flow_tb.fwd_init_winbytes != 108
		|| flow_tb.bwd_init_winbytes != 60)
	{
		printf("pkt4 test fail\n");
		return -1;
	}

	return 0;
}





#endif

