
#include "featuregroup2_decoder.h"


#define TH_FIN                               0x01
#define TH_SYN                               0x02
#define TH_RST                               0x04
#define TH_PUSH                              0x08
#define TH_ACK                               0x10
#define TH_URG                               0x20
/** Establish a new connection reducing window */
#define TH_ECN                               0x40
/** Echo Congestion flag */
#define TH_CWR                               0x80


static void fin_flag_count(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if (pkt->tcp_flags & TH_FIN)
	{
		flow_tb->fin_flag_pkts++;
	}
	
	return;
}

static void syn_flag_count(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if (pkt->tcp_flags & TH_SYN)
	{
		flow_tb->syn_flag_pkts++;
	}
	
	return;

}

static void rst_flag_count(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if (pkt->tcp_flags & TH_RST)
	{
		flow_tb->rst_flag_pkts++;
	}
	
	return;

}

static void psh_flag_count(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if (pkt->tcp_flags & TH_PUSH)
	{
		flow_tb->psh_flag_pkts++;
	}
	
	return;

}

static void ack_flag_count(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if (pkt->tcp_flags & TH_ACK)
	{
		flow_tb->ack_flag_pkts++;
	}
	
	return;

}

static void ece_flag_count(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if (pkt->tcp_flags & TH_ECN)
	{
		flow_tb->ece_flag_pkts++;
	}
	
	return;

}

static void urg_flag_count(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if (pkt->tcp_flags & TH_URG)
	{
		flow_tb->urg_flag_pkts++;
	}
	
	return;

}

static void cwr_flag_count(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if (pkt->tcp_flags & TH_CWR)
	{
		flow_tb->cwr_flag_pkts++;
	}
	
	return;

}

static void fwd_push_flag_count(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if ((DIR_FWD == pkt->pkt_dir) && (pkt->tcp_flags & TH_PUSH))
	{
		flow_tb->fwd_push_flag_num++;
	}

	return;
}

static void bwd_push_flag_count(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if ((DIR_BWD == pkt->pkt_dir) && (pkt->tcp_flags & TH_PUSH))
	{
		flow_tb->bwd_push_flag_num++;
	}

	return;	
}

static void fwd_urg_flag_count(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if ((DIR_FWD == pkt->pkt_dir) && (pkt->tcp_flags & TH_URG))
	{
		flow_tb->fwd_urg_flag_num++;
	}

	return;	
}

static void bwd_urg_flag_count(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if ((DIR_BWD == pkt->pkt_dir) && (pkt->tcp_flags & TH_URG))
	{
		flow_tb->bwd_urg_flag_num++;
	}

	return;		
}

static void fwd_hdr_total_bytes_count(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if (DIR_FWD == pkt->pkt_dir)
	{
		flow_tb->fwd_hdr_total_bytes += pkt->pkt_mac_hdr_len + pkt->pkt_ip_hdr_len + pkt->pkt_trans_hdr_len;
	}

	return;			
}

static void bwd_hdr_total_bytes_count(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if (DIR_BWD == pkt->pkt_dir)
	{
		flow_tb->bwd_hdr_total_bytes += pkt->pkt_mac_hdr_len + pkt->pkt_ip_hdr_len + pkt->pkt_trans_hdr_len;
	}

	return;		
}


static void fwd_segment_size_mean(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if ((DIR_FWD == pkt->pkt_dir) && (pkt->pkt_ipproto_id == 6))
	{
		uint16_t tcp_payload = pkt->ip_len - pkt->pkt_ip_hdr_len - pkt->pkt_trans_hdr_len;
		
		flow_tb->fwd_segment_size_avg = nidps_mean(flow_tb->fwd_segment_size_avg, flow_tb->total_fwd_tcp_pkts - 1, tcp_payload);
	}

	return;			
}

static void bwd_segment_size_mean(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if ((DIR_BWD == pkt->pkt_dir) && (pkt->pkt_ipproto_id == 6))
	{
		uint16_t tcp_payload = pkt->ip_len - pkt->pkt_ip_hdr_len - pkt->pkt_trans_hdr_len;
		
		flow_tb->bwd_segment_size_avg = nidps_mean(flow_tb->bwd_segment_size_avg, flow_tb->total_bwd_tcp_pkts - 1, tcp_payload);
	}

	return;		
}


static void fwd_iat_min_cal(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if ((DIR_FWD == pkt->pkt_dir) && !(fabs(flow_tb->last_fwd_pkt_arrive_time + 1.0) <= FLOAT_EPS))
	{
		double cur_time_iat = pkt->pkt_timestamp - flow_tb->last_fwd_pkt_arrive_time;

		if (fabs(flow_tb->fwd_iat_min - 0.0) <= FLOAT_EPS)
		{
			flow_tb->fwd_iat_min = cur_time_iat;
		}
		else
		{
			flow_tb->fwd_iat_min = nidps_min(flow_tb->fwd_iat_min, cur_time_iat);
		}
		
	}
	
	return;
}

static void fwd_iat_max_cal(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if ((DIR_FWD == pkt->pkt_dir) && !(fabs(flow_tb->last_fwd_pkt_arrive_time + 1.0) <= FLOAT_EPS))
	{
		double cur_time_iat = pkt->pkt_timestamp - flow_tb->last_fwd_pkt_arrive_time;
		
		flow_tb->fwd_iat_max = nidps_max(flow_tb->fwd_iat_max, cur_time_iat);
	}
	
	return;

}

static void fwd_iat_mean_cal(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if ((DIR_FWD == pkt->pkt_dir) && !(fabs(flow_tb->last_fwd_pkt_arrive_time + 1.0) <= FLOAT_EPS) )
	{
		double cur_time_iat = pkt->pkt_timestamp - flow_tb->last_fwd_pkt_arrive_time;
		
		flow_tb->fwd_iat_mean = nidps_mean(flow_tb->fwd_iat_mean, flow_tb->total_fwd_pkts - 2, cur_time_iat);
	}
	
	return;

}

static void fwd_iat_std_cal(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if ((DIR_FWD == pkt->pkt_dir) && !(fabs(flow_tb->last_fwd_pkt_arrive_time + 1.0) <= FLOAT_EPS) )
	{
		double cur_time_iat = pkt->pkt_timestamp - flow_tb->last_fwd_pkt_arrive_time;
		
		flow_tb->fwd_iat_std = nidps_std(flow_tb->fwd_iat_std, flow_tb->fwd_iat_mean, flow_tb->total_fwd_pkts - 2, cur_time_iat);   	
	}
	
	return;

}

static void fwd_iat_total_cal(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if ((DIR_FWD == pkt->pkt_dir) && !(fabs(flow_tb->last_fwd_pkt_arrive_time + 1.0) <= FLOAT_EPS) )
	{
		double cur_time_iat = pkt->pkt_timestamp - flow_tb->last_fwd_pkt_arrive_time;
		
		flow_tb->fwd_iat_total += cur_time_iat;   	
	}
	
	return;
}


static void bwd_iat_min_cal(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if ((DIR_BWD == pkt->pkt_dir) && !(fabs(flow_tb->last_bwd_pkt_arrive_time + 1.0) <= FLOAT_EPS) )
	{
		double cur_time_iat = pkt->pkt_timestamp - flow_tb->last_bwd_pkt_arrive_time;
		
		if (fabs(flow_tb->bwd_iat_min - 0.0) <= FLOAT_EPS)
		{
			flow_tb->bwd_iat_min = cur_time_iat;
		}
		else
		{
			flow_tb->bwd_iat_min = nidps_min(flow_tb->bwd_iat_min, cur_time_iat);
		}
		
	}
	
	return;
}

static void bwd_iat_max_cal(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if ((DIR_BWD == pkt->pkt_dir) && !(fabs(flow_tb->last_bwd_pkt_arrive_time + 1.0) <= FLOAT_EPS) )
	{
		double cur_time_iat = pkt->pkt_timestamp - flow_tb->last_bwd_pkt_arrive_time;
		
		flow_tb->bwd_iat_max = nidps_max(flow_tb->bwd_iat_max, cur_time_iat);
	}
	
	return;

}

static void bwd_iat_mean_cal(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if ((DIR_BWD == pkt->pkt_dir) && !(fabs(flow_tb->last_bwd_pkt_arrive_time + 1.0) <= FLOAT_EPS) )
	{
		double cur_time_iat = pkt->pkt_timestamp - flow_tb->last_bwd_pkt_arrive_time;
		
		flow_tb->bwd_iat_mean = nidps_mean(flow_tb->bwd_iat_mean, flow_tb->total_bwd_pkts - 2, cur_time_iat);
	}
	
	return;

}

static void bwd_iat_std_cal(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if ((DIR_BWD == pkt->pkt_dir) && !(fabs(flow_tb->last_bwd_pkt_arrive_time + 1.0) <= FLOAT_EPS) )
	{
		double cur_time_iat = pkt->pkt_timestamp - flow_tb->last_bwd_pkt_arrive_time;
		
		flow_tb->bwd_iat_std = nidps_std(flow_tb->bwd_iat_std, flow_tb->bwd_iat_mean, flow_tb->total_bwd_pkts - 2, cur_time_iat);   	
	}
	
	return;

}

static void bwd_iat_total_cal(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if ((DIR_BWD == pkt->pkt_dir) && !(fabs(flow_tb->last_bwd_pkt_arrive_time + 1.0) <= FLOAT_EPS) )
	{
		double cur_time_iat = pkt->pkt_timestamp - flow_tb->last_bwd_pkt_arrive_time;
		
		flow_tb->bwd_iat_total += cur_time_iat;   	
	}
	
	return;
}



static void flow_iat_min_cal(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if (!(fabs(flow_tb->last_pkt_arrive_time + 1.0) <= FLOAT_EPS))
	{
		double cur_time_iat = pkt->pkt_timestamp - flow_tb->last_pkt_arrive_time;

		if (fabs(flow_tb->flow_iat_min - 0.0) <= FLOAT_EPS)
		{
			flow_tb->flow_iat_min = cur_time_iat;
		}
		else
		{
			flow_tb->flow_iat_min = nidps_min(flow_tb->flow_iat_min, cur_time_iat);
		}
	}
	
	return;
}

static void flow_iat_max_cal(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if (!(fabs(flow_tb->last_pkt_arrive_time + 1.0) <= FLOAT_EPS))
	{
		double cur_time_iat = pkt->pkt_timestamp - flow_tb->last_pkt_arrive_time;
		
		flow_tb->flow_iat_max = nidps_max(flow_tb->flow_iat_max, cur_time_iat);
	}
	
	return;

}

static void flow_iat_mean_cal(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if (!(fabs(flow_tb->last_pkt_arrive_time + 1.0) <= FLOAT_EPS))
	{
		double cur_time_iat = pkt->pkt_timestamp - flow_tb->last_pkt_arrive_time;
		
		flow_tb->flow_iat_mean = nidps_mean(flow_tb->flow_iat_mean, flow_tb->total_bwd_pkts + flow_tb->total_fwd_pkts - 2, cur_time_iat);
	}
	
	return;

}

static void flow_iat_std_cal(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	if (!(fabs(flow_tb->last_pkt_arrive_time + 1.0) <= FLOAT_EPS))
	{
		float cur_time_iat = pkt->pkt_timestamp - flow_tb->last_pkt_arrive_time;
		
		flow_tb->flow_iat_std = nidps_std(flow_tb->flow_iat_std, flow_tb->flow_iat_mean, flow_tb->total_bwd_pkts + flow_tb->total_fwd_pkts - 2, cur_time_iat);   	
	}
	
	return;

}

void featuregroup2_init(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	uint32_t first_pkt_flg = 0;

	if (pkt->pkt_dir == DIR_FWD)
	{
		flow_tb->total_fwd_pkts++;
		if (flow_tb->total_fwd_pkts == 1)
		{
			first_pkt_flg = 1;
		}
		
		if (pkt->pkt_ipproto_id == 6)
		{
			flow_tb->total_fwd_tcp_pkts++;
		}

	}
	else
	{
		flow_tb->total_bwd_pkts++;
		if (flow_tb->total_bwd_pkts == 1)
		{
			first_pkt_flg = 1;
		}
				
		if (pkt->pkt_ipproto_id == 6)
		{
			flow_tb->total_bwd_tcp_pkts++;
		}
	}

	if (first_pkt_flg == 1)
	{
		if (pkt->pkt_dir == DIR_FWD)
		{
			flow_tb->last_fwd_pkt_arrive_time = -1;
			flow_tb->last_pkt_arrive_time = -1;
		}
		else
		{
			flow_tb->last_bwd_pkt_arrive_time = -1;
		}
	}

	return;
}

void featuregroup2_post_init(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	flow_tb->last_pkt_arrive_time = pkt->pkt_timestamp;
	
	if (pkt->pkt_dir == DIR_FWD)
	{
		flow_tb->last_fwd_pkt_arrive_time = pkt->pkt_timestamp;
	}
	else
	{
		flow_tb->last_bwd_pkt_arrive_time = pkt->pkt_timestamp;
	}
	
	return;
}


void featuregroup2_decode(PKT_STATS_INFO *pkt, LINK_STATS_INFO *flow_tb)
{
	featuregroup2_init(pkt, flow_tb);

	fin_flag_count(pkt, flow_tb);
	syn_flag_count(pkt, flow_tb);
	rst_flag_count(pkt, flow_tb);
	psh_flag_count(pkt, flow_tb);
	ack_flag_count(pkt, flow_tb);
	ece_flag_count(pkt, flow_tb);
	urg_flag_count(pkt, flow_tb);
	cwr_flag_count(pkt, flow_tb);

	fwd_push_flag_count(pkt, flow_tb);
	bwd_push_flag_count(pkt, flow_tb);
	fwd_urg_flag_count(pkt, flow_tb);
	bwd_urg_flag_count(pkt, flow_tb);

	fwd_hdr_total_bytes_count(pkt, flow_tb);
	bwd_hdr_total_bytes_count(pkt, flow_tb);

	fwd_segment_size_mean(pkt, flow_tb);
	bwd_segment_size_mean(pkt, flow_tb);

	/** fixed-order */
	fwd_iat_min_cal(pkt, flow_tb);
	fwd_iat_max_cal(pkt, flow_tb);
	fwd_iat_std_cal(pkt, flow_tb);
	fwd_iat_mean_cal(pkt, flow_tb);
	fwd_iat_total_cal(pkt, flow_tb);

	/** fixed-order */
	bwd_iat_min_cal(pkt, flow_tb);
	bwd_iat_max_cal(pkt, flow_tb);
	bwd_iat_std_cal(pkt, flow_tb);
	bwd_iat_mean_cal(pkt, flow_tb);
	bwd_iat_total_cal(pkt, flow_tb);

	/** fixed-order */
	flow_iat_min_cal(pkt, flow_tb);
	flow_iat_max_cal(pkt, flow_tb);
	flow_iat_std_cal(pkt, flow_tb);
	flow_iat_mean_cal(pkt, flow_tb);

	featuregroup2_post_init(pkt, flow_tb);
	
	return;
}






#if 0
#define UNITTESTS
#endif

#ifdef UNITTESTS

#include <stdio.h>

#include <string.h>



int test()
{
	PKT_STATS_INFO pkt;
	LINK_STATS_INFO flow_tb;
	memset(&pkt, 0 , sizeof(PKT_STATS_INFO));
	memset(&flow_tb, 0 , sizeof(LINK_STATS_INFO));

	pkt.pkt_dir = DIR_FWD;
	pkt.pkt_len = 60;
	pkt.ip_len = 52;
	pkt.pkt_mac_hdr_len = 8;
	pkt.pkt_ip_hdr_len = 20;
	pkt.pkt_trans_hdr_len = 20;
	pkt.tcp_flags = TH_SYN;
	pkt.pkt_timestamp = 1.1;
	pkt.pkt_ipproto_id = 6;

	featuregroup2_decode(&pkt, &flow_tb);
	
	if (flow_tb.fin_flag_pkts != 0
		|| flow_tb.syn_flag_pkts != 1
		|| flow_tb.rst_flag_pkts != 0
		|| flow_tb.psh_flag_pkts != 0
		|| flow_tb.ack_flag_pkts != 0
		|| flow_tb.ece_flag_pkts != 0
		|| flow_tb.urg_flag_pkts != 0
		|| flow_tb.cwr_flag_pkts != 0
		|| flow_tb.fwd_push_flag_num != 0
		|| flow_tb.bwd_push_flag_num != 0
		|| flow_tb.fwd_urg_flag_num != 0
		|| flow_tb.bwd_urg_flag_num != 0
		|| flow_tb.fwd_hdr_total_bytes != 48
		|| flow_tb.bwd_hdr_total_bytes != 0
		|| flow_tb.fwd_segment_size_avg != 12
		|| flow_tb.bwd_segment_size_avg != 0
		|| flow_tb.fwd_iat_min != 0
		|| flow_tb.fwd_iat_max != 0
		|| flow_tb.fwd_iat_mean != 0
		|| flow_tb.fwd_iat_std != 0
		|| flow_tb.fwd_iat_total != 0
		|| flow_tb.bwd_iat_min != 0
		|| flow_tb.bwd_iat_max != 0
		|| flow_tb.bwd_iat_mean != 0
		|| flow_tb.bwd_iat_std != 0
		|| flow_tb.bwd_iat_total != 0
		|| flow_tb.flow_iat_min != 0
		|| flow_tb.flow_iat_max != 0
		|| flow_tb.flow_iat_mean != 0
		|| flow_tb.flow_iat_std != 0)
	{
		printf("pkt1 test fail\n");
		return -1;
	}


	pkt.pkt_dir = DIR_BWD;
	pkt.pkt_len = 60;
	pkt.ip_len = 52;
	pkt.pkt_mac_hdr_len = 8;
	pkt.pkt_ip_hdr_len = 20;
	pkt.pkt_trans_hdr_len = 20;
	pkt.tcp_flags = TH_ACK;
	pkt.pkt_timestamp = 1.3;
	pkt.pkt_ipproto_id = 6;

	featuregroup2_decode(&pkt, &flow_tb);
	float cc = fabs(flow_tb.flow_iat_mean - 0.2);
	float bb = fabs(flow_tb.flow_iat_min - 0.2);
	
	if (flow_tb.fin_flag_pkts != 0
		|| flow_tb.syn_flag_pkts != 1
		|| flow_tb.rst_flag_pkts != 0
		|| flow_tb.psh_flag_pkts != 0
		|| flow_tb.ack_flag_pkts != 1
		|| flow_tb.ece_flag_pkts != 0
		|| flow_tb.urg_flag_pkts != 0
		|| flow_tb.cwr_flag_pkts != 0
		|| flow_tb.fwd_push_flag_num != 0
		|| flow_tb.bwd_push_flag_num != 0
		|| flow_tb.fwd_urg_flag_num != 0
		|| flow_tb.bwd_urg_flag_num != 0
		|| flow_tb.fwd_hdr_total_bytes != 48
		|| flow_tb.bwd_hdr_total_bytes != 48
		|| flow_tb.fwd_segment_size_avg != 12
		|| flow_tb.bwd_segment_size_avg != 12
		|| flow_tb.fwd_iat_min != 0
		|| flow_tb.fwd_iat_max != 0
		|| flow_tb.fwd_iat_mean != 0
		|| flow_tb.fwd_iat_std != 0
		|| flow_tb.fwd_iat_total != 0
		|| flow_tb.bwd_iat_min != 0
		|| flow_tb.bwd_iat_max != 0
		|| flow_tb.bwd_iat_mean != 0
		|| flow_tb.bwd_iat_std != 0
		|| flow_tb.bwd_iat_total != 0
		|| !(fabs(flow_tb.flow_iat_min - 0.2) <= FLOAT_EPS)
		|| !(fabs(flow_tb.flow_iat_max - 0.2) <= FLOAT_EPS)
		|| !(fabs(flow_tb.flow_iat_mean - 0.2) <= FLOAT_EPS)
		|| !(fabs(flow_tb.flow_iat_std - 0.0) <= FLOAT_EPS))
	{
		printf("pkt2 test fail\n");
		return -1;
	}


	pkt.pkt_dir = DIR_FWD;
	pkt.pkt_len = 60;
	pkt.ip_len = 52;
	pkt.pkt_mac_hdr_len = 8;
	pkt.pkt_ip_hdr_len = 20;
	pkt.pkt_trans_hdr_len = 20;
	pkt.tcp_flags = TH_PUSH;
	pkt.pkt_timestamp = 1.6;
	pkt.pkt_ipproto_id = 6;

	featuregroup2_decode(&pkt, &flow_tb);
	
	if (flow_tb.fin_flag_pkts != 0
		|| flow_tb.syn_flag_pkts != 1
		|| flow_tb.rst_flag_pkts != 0
		|| flow_tb.psh_flag_pkts != 1
		|| flow_tb.ack_flag_pkts != 1
		|| flow_tb.ece_flag_pkts != 0
		|| flow_tb.urg_flag_pkts != 0
		|| flow_tb.cwr_flag_pkts != 0
		|| flow_tb.fwd_push_flag_num != 1
		|| flow_tb.bwd_push_flag_num != 0
		|| flow_tb.fwd_urg_flag_num != 0
		|| flow_tb.bwd_urg_flag_num != 0
		|| flow_tb.fwd_hdr_total_bytes != 96
		|| flow_tb.bwd_hdr_total_bytes != 48
		|| flow_tb.fwd_segment_size_avg != 12
		|| flow_tb.bwd_segment_size_avg != 12
		|| !(fabs(flow_tb.fwd_iat_min - 0.5) <= FLOAT_EPS)
		|| !(fabs(flow_tb.fwd_iat_max - 0.5) <= FLOAT_EPS)
		|| !(fabs(flow_tb.fwd_iat_mean - 0.5) <= FLOAT_EPS)
		|| !(fabs(flow_tb.fwd_iat_std - 0.0) <= FLOAT_EPS)
		|| !(fabs(flow_tb.fwd_iat_total - 0.5) <= FLOAT_EPS)
		|| flow_tb.bwd_iat_min != 0
		|| flow_tb.bwd_iat_max != 0
		|| flow_tb.bwd_iat_mean != 0
		|| flow_tb.bwd_iat_std != 0
		|| flow_tb.bwd_iat_total != 0
		|| !(fabs(flow_tb.flow_iat_min - 0.2) <= FLOAT_EPS)
		|| !(fabs(flow_tb.flow_iat_max - 0.3) <= FLOAT_EPS)
		|| !(fabs(flow_tb.flow_iat_mean - 0.25) <= FLOAT_EPS)
		|| !(fabs(flow_tb.flow_iat_std - 0.05) <= FLOAT_EPS))
	{

		printf("pkt3 test fail\n");
		return -1;
	}
		
	return 0;
}






#endif

