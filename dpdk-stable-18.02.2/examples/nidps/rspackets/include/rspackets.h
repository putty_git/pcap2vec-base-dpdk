#ifndef _RSPACKETS_H_
#define _RSPACKETS_H_
/*---------------head file start--------------------------------------*/
#include "managejob.h"
#include "pcap_reader.h"
#include "nidps_per_lcore.h"
/*---------------head file end----------------------------------------*/
/*-----------------------dpdk head file start--------------------*/
/*-----------------------dpdk head file end--------------------*/

#define P2V_MAX_PKT_BURST 64
int
main_loop(__attribute__((unused)) void *dummy);
int p2vRcvPktFrPcapFile(unsigned char *ptUseData, const struct pcap_pkthdr *pkthdr, const uint8_t * pkt);
int p2vRxburstFromPcap(pcap_t * ptPcapFile, struct rte_mbuf **pkts_burst,
                             struct rte_mempool* ptMp, int *pPktNum );
int p2vWorkFlowStart(p2v_file_node *ptFileNode);
void p2vSwapFiveTupleInfov4(FIVE_TUPLE_KEY * ptFiveTupleInfo);
void p2vSwapFiveTupleInfov6(FIVE_TUPLE_KEY * ptFiveTupleInfo);
void p2vSwapTupleInfo(FIVE_TUPLE_KEY *ptTupleInfo, int ipVer);

void p2vTcpFlowStatus(p2v_flow_cnt *ptFlowData, uint8_t tcpflg, uint8_t pktDir);
int p2vPktProcess(struct rte_mbuf **pptPktsBurst, int pktNum, struct rte_hash *ptFlowTbl);
void p2vFlowTblClr( struct rte_hash *h);
int p2vFlowTblAge(uint32_t curIndex, struct rte_hash *h);
void updateFlowCnt(p2v_flow_cnt *ptFlowData, PKT_STATS_INFO *ptPktStatsInfo);
void p2vOutputEigenvalToBuf(p2v_flow_cnt *ptFlowData);

#endif
