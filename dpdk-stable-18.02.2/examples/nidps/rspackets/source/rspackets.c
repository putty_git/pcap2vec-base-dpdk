#include "rspackets.h"
#include "pcap_reader.h"
#include "layer_2_to_4_parse.h"
#include "featuregroup2_decoder.h"
#include "featuregroup1_decoder.h"
#include "featuregroup3_decoder.h"


#include "rte_malloc.h"



typedef struct rte_allc_pktbuf
{
   struct rte_mempool* ptMp;
   struct rte_mbuf ** pptMbuf ;
   int index ;
   int rsv;
}p2v_allc_pktbuf;

RTE_DEFINE_PER_LCORE(p2v_threadvar_desc, p2vThrVarDes);


int  p2vRcvPktFrPcapFile(unsigned char *ptUseData, const struct pcap_pkthdr *pkthdr, const uint8_t * pkt)
{
    p2v_allc_pktbuf  *ptAllcPktBuf = (p2v_allc_pktbuf*)ptUseData;
    struct rte_mempool * ptMp = ptAllcPktBuf->ptMp;
    struct rte_mbuf * ptPktMbuf   = NULL;
    if (pkthdr->caplen)
    {
        ptPktMbuf =  rte_pktmbuf_alloc(ptMp);
        if (NULL == ptPktMbuf)
        {
            printf("no enough memory! \n");
            return P2V_FAIL;
        }
        ptPktMbuf->data_len = pkthdr->len;
        if (pkthdr->caplen < pkthdr->len)
        {
            //print_data(pkt, pkthdr->caplen);
            ptPktMbuf->data_len = pkthdr->caplen;
        }
        ptPktMbuf->data_off = sizeof(struct pcap_pkthdr);
        
        memcpy((ptPktMbuf->buf_addr), pkthdr, ptPktMbuf->data_off);

        if (ptPktMbuf->data_len > 1600)
        {
            ptPktMbuf->data_len = 1600;
        }

        memcpy((ptPktMbuf->buf_addr + ptPktMbuf->data_off), pkt, ptPktMbuf->data_len);
        ptPktMbuf->buf_len = ptPktMbuf->data_len + ptPktMbuf->data_off;
        ptPktMbuf->pkt_len  = ptPktMbuf->data_len;  
        ptAllcPktBuf->pptMbuf[ptAllcPktBuf->index]= ptPktMbuf;
        ptAllcPktBuf->index ++;
        //printf ("per 64 ,cur index %u\n", ptAllcPktBuf->index);
        //print_data((ptPktMbuf->buf_addr + ptPktMbuf->data_off), 64);
    }
    //printf("===============================\n");

    return P2V_SUCCESS;
}

int p2vRxburstFromPcap(pcap_t * ptPcapFile, struct rte_mbuf **pkts_burst,
                             struct rte_mempool* ptMp, int *pPktNum )
{

    p2v_allc_pktbuf tAllocPktBuf;
    tAllocPktBuf.pptMbuf = pkts_burst;
    tAllocPktBuf.ptMp = ptMp;
    tAllocPktBuf.index = 0;
    int ret = 0;
    ret = pcap_loop(ptPcapFile, P2V_MAX_PKT_BURST, p2vRcvPktFrPcapFile, &tAllocPktBuf);
    *pPktNum = tAllocPktBuf.index ;

    if (-1 == ret)
	{
		printf("%s", pcap_geterr(ptPcapFile));
		return P2V_FAIL;
	}
    if (P2V_MAX_PKT_BURST == pPktNum)
    {
        printf("per 64 pkt one read!\n");
    }
    return P2V_SUCCESS;
}

void p2vFreeMbuf(struct rte_mbuf ** ptks_burst, int pktNum)
{
    int index ;
    for(index = 0; index  < pktNum; index ++)
    {     
        rte_pktmbuf_free(ptks_burst[index]);
    }
    return ;
}

void p2vSwapFiveTupleInfov4(FIVE_TUPLE_KEY * ptFiveTupleInfo)
{
    u16 src_port    = ptFiveTupleInfo->src_port;
    u16 dst_port    = ptFiveTupleInfo->dst_port;
    u32 src_ip_v4   = ptFiveTupleInfo->ip2tuple.ipv4_2tuple.srcip_v4;
    u32  dst_ip_v4  = ptFiveTupleInfo->ip2tuple.ipv4_2tuple.dstip_v4;

    ptFiveTupleInfo->ip2tuple.ipv4_2tuple.srcip_v4 = dst_ip_v4;
    ptFiveTupleInfo->ip2tuple.ipv4_2tuple.dstip_v4 = src_ip_v4;
    ptFiveTupleInfo->src_port = dst_port;
    ptFiveTupleInfo->dst_port = src_port;
    return ;
}

void p2vSwapFiveTupleInfov6(FIVE_TUPLE_KEY * ptFiveTupleInfo)
{
    u16 src_port    = ptFiveTupleInfo->src_port;
    u16 dst_port    = ptFiveTupleInfo->dst_port;
    uint8_t src_ip_v6[16] = {0};
    memcpy(src_ip_v6, ptFiveTupleInfo->ip2tuple.ipv6_2tuple.srcip_v6, 16);
    uint8_t dst_ip_v6[16] = {0};
    memcpy(dst_ip_v6, ptFiveTupleInfo->ip2tuple.ipv6_2tuple.dstip_v6, 16);

    memcpy(ptFiveTupleInfo->ip2tuple.ipv6_2tuple.srcip_v6, dst_ip_v6, 16);
    memcpy(ptFiveTupleInfo->ip2tuple.ipv6_2tuple.dstip_v6, src_ip_v6, 16);
    ptFiveTupleInfo->src_port = dst_port;
    ptFiveTupleInfo->dst_port = src_port;
    return ;
}


void p2vSwapTupleInfo(FIVE_TUPLE_KEY *ptTupleInfo, int ipVer)
{
    /*1. swap ip + port*/
    if (IPV4 == ipVer)
    {
        p2vSwapFiveTupleInfov4(ptTupleInfo);
    }
    else
    {
        p2vSwapFiveTupleInfov6(ptTupleInfo);
    }
    return;
}

void p2vTcpFlowStatus(p2v_flow_cnt *ptFlowData, uint8_t tcpflg, uint8_t pktDir)
{
    p2v_flow_close_status *ptFlwStu = &(ptFlowData->tFlowClsStus);
    if ((tcpflg & TCP_FIN_FLAG) != 0)
    {
        if (DIR_FWD == pktDir)
        {
            if (0 == ptFlwStu->fwdClsStatus)
            {
                ptFlwStu->fwdClsStatus = FIN_WAIT_1;
            }
            if ((FIN_WAIT_1 == ptFlwStu->bwdClsStatus )
                && ((tcpflg & TCP_ACK_FLAG) == TCP_ACK_FLAG))
            {
                ptFlwStu->bwdClsStatus = FIN_WAIT_TIME;
            }
        }
        else
        {
            if (0 == ptFlwStu->bwdClsStatus)
            {
                ptFlwStu->bwdClsStatus = FIN_WAIT_1;
            }
            if ((FIN_WAIT_1 == ptFlwStu->fwdClsStatus )
                && ((tcpflg & TCP_ACK_FLAG) == TCP_ACK_FLAG))
            {
                ptFlwStu->fwdClsStatus = FIN_WAIT_TIME;
            }
        }
    }
    else if ((tcpflg & TCP_ACK_FLAG) != 0)
    {
        if (DIR_FWD == pktDir)
        {
            if (FIN_WAIT_1 == ptFlwStu->bwdClsStatus )
            {
                ptFlwStu->bwdClsStatus = FIN_WAIT_2;
            }
            else if (FIN_WAIT_TIME == ptFlwStu->bwdClsStatus )
            {
                ptFlwStu->bwdClsStatus = TCP_CLOSE;
                ptFlwStu->fwdClsStatus = TCP_CLOSE;
                ptFlwStu->tcpClsStatus = TCP_CLOSE;
            }
        }
        else
        {
            if (FIN_WAIT_1 == ptFlwStu->fwdClsStatus )
            {
                ptFlwStu->fwdClsStatus = FIN_WAIT_2;
            }
            else if (FIN_WAIT_TIME == ptFlwStu->fwdClsStatus )
            {
                ptFlwStu->bwdClsStatus = TCP_CLOSE;
                ptFlwStu->fwdClsStatus = TCP_CLOSE;
                ptFlwStu->tcpClsStatus = TCP_CLOSE;
            }
        }
        if (ptFlwStu->bwdClsStatus == FIN_WAIT_2 
                && ptFlwStu->fwdClsStatus == FIN_WAIT_2)
        {
            
            ptFlwStu->bwdClsStatus = TCP_CLOSE;
            ptFlwStu->fwdClsStatus = TCP_CLOSE;
            ptFlwStu->tcpClsStatus = TCP_CLOSE;
        }
    }
    else if ((tcpflg & TCP_RST_FLAG) != 0)
    {
        if (0 == ptFlwStu->tcpClsStatus)
        {
            ptFlwStu->tcpClsStatus = TCP_RST; 
        }
    }
    return ;
}

void updateFlowCnt(p2v_flow_cnt *ptFlowData, PKT_STATS_INFO *ptPktStatsInfo)
{
    /*1. 调用特征组1*/
    featuregroup1_decode(ptPktStatsInfo, &(ptFlowData->tLinkStatsInfo));
    /*2. 调用特征组2*/
    featuregroup2_decode(ptPktStatsInfo, &(ptFlowData->tLinkStatsInfo));
    /*3. 调用特征组3*/
    featuregroup3_decode(ptPktStatsInfo, &(ptFlowData->tLinkStatsInfo));
    return ;
}
int p2vPktProcess(struct rte_mbuf **pptPktsBurst, int pktNum, struct rte_hash *ptFlowTbl)
{
    int index ;
    int ret;
    struct rte_mbuf *ptPktMbuf = NULL;
    
    p2v_threadvar_desc *ptThrVarDes = &(P2V_GET_THRVAEDES_LCORE_PARA());
    for(index = 0; index < pktNum; index++)
    {
        ptPktMbuf = pptPktsBurst[index];
        /*1. 数据包字段解析*/

        uint8_t *pkt = ptPktMbuf->buf_addr + ptPktMbuf->data_off;
        struct pcap_pkthdr *ptPktThdr =(struct pcap_pkthdr *)ptPktMbuf->buf_addr;
        
        FIVE_TUPLE_KEY tuple_info;
        memset(&tuple_info, 0x0, sizeof(FIVE_TUPLE_KEY));
        PKT_STATS_INFO pkt_stats_info;
        memset(&pkt_stats_info, 0x0, sizeof(PKT_STATS_INFO));
	    unsigned int realLen = ptPktMbuf->data_len;
        ret = ParseL2ToL4LayerProtoInfo(pkt,  ptPktThdr, &tuple_info, &pkt_stats_info, realLen);
        if (0 != ret)
        {
            printf("pkt parse fail, erro code %d\n", ret);
            ptThrVarDes->parsePktsErrCount++;
            continue;
        }
        /*2. 查询/建立流表*/
        
        FIVE_TUPLE_KEY tupleInfoSecond = {0};
        p2v_flow_cnt *ptFlowData = NULL;
        int secondQFlowTblFlg = 0;
        u32  ret = SelectDbTblByKey(ptFlowTbl, (void *)&tuple_info, (void **)&ptFlowData);
        if (-ENOENT == ret)
        {
            /*2.0 第一次查询失败，尝试交换五元组方向，再次查询*/            
            memcpy(&tupleInfoSecond, &tuple_info, sizeof(FIVE_TUPLE_KEY));
            p2vSwapTupleInfo(&tupleInfoSecond, pkt_stats_info.pkt_ip_version);            
            ret = SelectDbTblByKey(ptFlowTbl, (void *)&tupleInfoSecond, (void **)&ptFlowData);
            if (-ENOENT != ret)
            {
                secondQFlowTblFlg = 1;
            }
            else
            {
            
                uint32_t flowRdNum = rte_hash_count(ptFlowTbl);
                if (flowRdNum > ((ptThrVarDes->flowTblSz * 4) / 5))
                {
                    
                    printf("flow %s records %u!\n", ptThrVarDes->ptFlwTblName, flowRdNum);
                    ptThrVarDes->flowTblAgeIndex = p2vFlowTblAge(ptThrVarDes->flowTblAgeIndex, ptThrVarDes->ptFlowTbl);
                }
                /*2.1 二次查询失败则创建流表*/
                ptFlowData = rte_zmalloc_socket("p2v_flow_cnt", sizeof(p2v_flow_cnt), RTE_CACHE_LINE_SIZE, rte_socket_id());
                /*补充赋值过程*/
                
                ret = InsertDbTblByKey(ptFlowTbl, &tuple_info, (void *)ptFlowData);
                if(P2V_SUCCESS != ret)
                {
                    
                     /*0. 每次工作流开始时检查流表负荷是否达到80%?
                       * 达到，则执行老化每次扫描256哥, 循环扫描老化
                     */
                    uint32_t flowRdNum = rte_hash_count(ptFlowTbl);
                    printf("flow record create fail, flow records %u!\n", flowRdNum);
                    ptThrVarDes->flwTblCrtFailPkts++;
                    return ret;
                }
                ptThrVarDes->flowRdCrtCount++;
                ret = SelectDbTblByKey(ptFlowTbl, &tuple_info, (void **)&ptFlowData);
                if(-ENOENT == ret)
                {
                    /*增加统计与失败统计*/           
                    printf("flow query fail!\n");
                    return ret;
                }
                gettimeofday( &(ptFlowData->crtTime), NULL);
                memcpy(ptFlowData->flowinPcapName, ptThrVarDes->ptInPcapName,P2V_NAME_LEN);
            }
        }
        if ((pkt_stats_info.tcp_flags & TCP_FLAG_ALL) == TCP_SYN_FLAG)
        {
            pkt_stats_info.pkt_dir = DIR_FWD;
        }
        if ( 1 == secondQFlowTblFlg)
        {
            pkt_stats_info.pkt_dir = DIR_BWD;
        }
        /*2.3 如果是fin标志要更新tcp流标志，删除记录输出结果*/
        
        p2vTcpFlowStatus(ptFlowData, pkt_stats_info.tcp_flags, pkt_stats_info.pkt_dir);

        if ( TCP_CLOSE == ptFlowData->tFlowClsStus.tcpClsStatus)
        {
            
            updateFlowCnt(ptFlowData, &pkt_stats_info);
            /*2.4 将此流表记录特征值输出到输出队列*/
        
            p2vOutputEigenvalToBuf(ptFlowData);
            /*2.5 删除流表记录*/
            rte_free(ptFlowData);
            FIVE_TUPLE_KEY *ptDelRecordKey = &tuple_info;
            if (1 == secondQFlowTblFlg)
            {
                ptDelRecordKey = &tupleInfoSecond;
            }
            
            DeleteDbTblByKey(ptFlowTbl, ptDelRecordKey);
            ptThrVarDes->flwNrmDelCount++;
            ptThrVarDes->flowRdDelCount++;
            continue;
        }
        /*2.5 更新流表记录，这里根据特征值整合,暂不处理*/
            
        updateFlowCnt(ptFlowData, &pkt_stats_info);
        
    }
    return P2V_SUCCESS;
}

void p2vOutputEigenvalToBuf(p2v_flow_cnt *ptFlowData)
{
    p2v_threadvar_desc       *ptThrVarDes = &(P2V_GET_THRVAEDES_LCORE_PARA());
    p2v_output_eigenval_buf  *ptOutputBuf = ptThrVarDes->ptOutputEigenvalBuf;
    unsigned int              count = ptOutputBuf->outCount ;  

    p2v_eigenval             *ptEigenval  = &(ptOutputBuf->tEigenvals[count]);
    LINK_STATS_INFO          *ptLinkStatsInfo = &(ptFlowData->tLinkStatsInfo);
    /*================超长特征赋值==========================*/
    ptEigenval->flow_duration               = ptLinkStatsInfo->flow_duration_time;
    ptEigenval->total_fwd_packets           = ptLinkStatsInfo->total_fwd_pkt;
    ptEigenval->total_bwd_packets           = ptLinkStatsInfo->total_bwd_pkt;
    ptEigenval->total_length_of_bwd_packet  = ptLinkStatsInfo->total_bwd_bytes;
    ptEigenval->total_length_of_fwd_packet  = ptLinkStatsInfo->total_fwd_bytes;
    ptEigenval->fwd_packet_length_min       = ptLinkStatsInfo->min_fwd_bytes;
    ptEigenval->fwd_packet_length_max       = ptLinkStatsInfo->max_fwd_bytes;
    ptEigenval->fwd_packet_length_mean      = ptLinkStatsInfo->mean_fwd_bytes;
    ptEigenval->fwd_packet_length_std       = ptLinkStatsInfo->std_fwd_bytes;
    ptEigenval->bwd_packet_length_min       = ptLinkStatsInfo->min_bwd_bytes;
    ptEigenval->bwd_packet_length_max       = ptLinkStatsInfo->max_bwd_bytes;
    ptEigenval->bwd_packet_length_mean      = ptLinkStatsInfo->mean_bwd_bytes;
    ptEigenval->bwd_packet_length_std       = ptLinkStatsInfo->std_bwd_bytes;
    ptEigenval->flow_bytes_per_second       = ptLinkStatsInfo->flow_bytes_second;
    ptEigenval->flow_packets_per_second     = ptLinkStatsInfo->flow_pkts_second;//15
    ptEigenval->flow_iat_mean               = ptLinkStatsInfo->flow_iat_mean;
    ptEigenval->flow_iat_std                = ptLinkStatsInfo->flow_iat_std;
    ptEigenval->flow_iat_max                = ptLinkStatsInfo->flow_iat_max;
    ptEigenval->flow_iat_min                = ptLinkStatsInfo->flow_iat_min;  
    ptEigenval->fwd_iat_min                 = ptLinkStatsInfo->fwd_iat_min;
    ptEigenval->fwd_iat_max                 = ptLinkStatsInfo->fwd_iat_max;
    ptEigenval->fwd_iat_mean                = ptLinkStatsInfo->fwd_iat_mean;
    ptEigenval->fwd_iat_std                 = ptLinkStatsInfo->fwd_iat_std;
    ptEigenval->fwd_iat_total               = ptLinkStatsInfo->fwd_iat_total;//24    
    ptEigenval->bwd_iat_min                 = ptLinkStatsInfo->bwd_iat_min;
    ptEigenval->bwd_iat_max                 = ptLinkStatsInfo->bwd_iat_max;
    ptEigenval->bwd_iat_mean                = ptLinkStatsInfo->bwd_iat_mean;
    ptEigenval->bwd_iat_std                 = ptLinkStatsInfo->bwd_iat_std;
    ptEigenval->bwd_iat_total               = ptLinkStatsInfo->bwd_iat_total;
    ptEigenval->fwd_psh_flags               = ptLinkStatsInfo->fwd_push_flag_num;
    ptEigenval->bwd_psh_flags               = ptLinkStatsInfo->bwd_push_flag_num;
    ptEigenval->fwd_urg_flags               = ptLinkStatsInfo->fwd_urg_flag_num;
    ptEigenval->bwd_urg_flags               = ptLinkStatsInfo->bwd_urg_flag_num;
    ptEigenval->fwd_header_length           = ptLinkStatsInfo->fwd_hdr_total_bytes;
    ptEigenval->bwd_header_length           = ptLinkStatsInfo->bwd_hdr_total_bytes;
    ptEigenval->fwd_packets_per_second      = ptLinkStatsInfo->fwd_pkts_second;
    ptEigenval->bwd_packets_per_second      = ptLinkStatsInfo->bwd_pkts_second;
    ptEigenval->packet_length_min           = ptLinkStatsInfo->min_pkts_len;
    ptEigenval->packet_length_max           = ptLinkStatsInfo->max_pkts_len;
    ptEigenval->packet_length_mean          = ptLinkStatsInfo->mean_pkts_len;
    ptEigenval->packet_length_std           = ptLinkStatsInfo->std_pkts_len;
    ptEigenval->packet_length_variance      = ptLinkStatsInfo->variance_pkts_len;
    ptEigenval->fin_flag_count              = ptLinkStatsInfo->fin_flag_pkts;
    ptEigenval->syn_flag_count              = ptLinkStatsInfo->syn_flag_pkts;
    ptEigenval->rst_flag_count              = ptLinkStatsInfo->rst_flag_pkts;
    ptEigenval->psh_flag_count              = ptLinkStatsInfo->psh_flag_pkts;
    ptEigenval->ack_flag_count              = ptLinkStatsInfo->ack_flag_pkts;
    ptEigenval->urg_flag_count              = ptLinkStatsInfo->urg_flag_pkts;
    ptEigenval->cwr_flag_count              = ptLinkStatsInfo->cwr_flag_pkts;
    ptEigenval->ece_flag_count              = ptLinkStatsInfo->ece_flag_pkts;
    ptEigenval->average_packet_size         = ptLinkStatsInfo->average_packet_size;
    ptEigenval->fwd_segment_size_avg        = ptLinkStatsInfo->fwd_segment_size_avg;
    ptEigenval->bwd_segment_size_avg        = ptLinkStatsInfo->bwd_segment_size_avg;
    ptEigenval->down_vs_up_ratio            = ptLinkStatsInfo->down_up_ratio;
    ptEigenval->fwd_init_win_bytes          = ptLinkStatsInfo->fwd_init_winbytes;
    ptEigenval->bwd_init_win_bytes          = ptLinkStatsInfo->bwd_init_winbytes;
    ptEigenval->fwd_act_data_pkts           = ptLinkStatsInfo->fwd_act_data_pkts;
    ptEigenval->fwd_seg_size_min            = ptLinkStatsInfo->fwd_seg_size_min;
    ptEigenval->active_min                  = ptLinkStatsInfo->active_min;
    ptEigenval->active_mean                 = ptLinkStatsInfo->active_mean;
    ptEigenval->active_max                  = ptLinkStatsInfo->active_max;
    ptEigenval->active_std                  = ptLinkStatsInfo->active_std;
    ptEigenval->idle_min                    = ptLinkStatsInfo->idle_min;
    ptEigenval->idle_mean                   = ptLinkStatsInfo->idle_mean;
    ptEigenval->idle_max                    = ptLinkStatsInfo->idle_max;
    ptEigenval->idle_std                    = ptLinkStatsInfo->idle_std;
    memcpy(ptEigenval->outputPcapFileName, ptFlowData->flowinPcapName, P2V_NAME_LEN - 1);
    ptEigenval->outputPcapFileName[P2V_NAME_LEN - 1] = '\0';
    /*=====================End================================*/

    
    /*1. 如果达到128个则输出*/
    count ++;
    int index;
    if(P2V_OUTPUT_BUF_SIZE == count)
    {
        for (index = 0; index < P2V_OUTPUT_BUF_SIZE ; index ++)
        {
            WriteFlowStatsInfoToCsvFile(ptOutputBuf->ptOutFile, &(ptOutputBuf->tEigenvals[index]));
        }
    }
    ptOutputBuf->outCount = count % P2V_OUTPUT_BUF_SIZE;
    return;
    
}
int p2vFlowTblAge(uint32_t curIndex, struct rte_hash *h)
{
    uint32_t iter_next = 0;
    const void *next_key = NULL;
    void *next_data = NULL;
    uint32_t delcount = 0;
    p2v_flow_cnt * ptFlowCnt = NULL;
    struct timeval  end;
    gettimeofday( &end, NULL );
    
    p2v_threadvar_desc *ptThrVarDes = &(P2V_GET_THRVAEDES_LCORE_PARA());
    
    u32        difTime = ptThrVarDes->flowTblAgeTime;
    while((rte_hash_iterate(h, &next_key, &next_data, &curIndex) >= 0) 
        && (delcount < RTE_MIN (ptThrVarDes->flowTblSz,1024)))
    {
        
        curIndex = curIndex % ptThrVarDes->flowTblSz;
        if (next_key != NULL)
        {       
            ptFlowCnt  = (p2v_flow_cnt *)next_data;
            if (ptFlowCnt == NULL)
            {
                /*2. 删除记录*/
                DeleteDbTblByKey(h, next_key);
                ptThrVarDes->flowRdDelCount++;
                delcount ++;
                continue;
                
            }
            int timeuse = 1000000 * ( end.tv_sec - ptFlowCnt->crtTime.tv_sec ) 
                            + end.tv_usec - ptFlowCnt->crtTime.tv_usec;

            printf("flow survice time %u\n", timeuse);   

            if ((timeuse >= difTime*1000)
                ||((ptFlowCnt->tFlowClsStus.bwdClsStatus > 0
                    ||ptFlowCnt->tFlowClsStus.fwdClsStatus>0
                    ||ptFlowCnt->tFlowClsStus.tcpClsStatus>0)
                   &&(timeuse >= 3*1000)))
            {
                /*1. 输出结果*/
                if (ptFlowCnt!=NULL)
                {
                    p2vOutputEigenvalToBuf(ptFlowCnt);
                    rte_free(ptFlowCnt);
                }
                /*2. 删除记录*/
                DeleteDbTblByKey(h, next_key);
                ptThrVarDes->flowRdDelCount++;
            }
        }
        
        delcount ++;
    }
    return curIndex % ptThrVarDes->flowTblSz;
}


void p2vFlowTblClr( struct rte_hash *h)
{
    uint32_t iter_next = 0;
    const void *next_key = NULL;
    void *next_data = NULL;
    p2v_flow_cnt * ptFlowCnt = NULL;
    uint32_t curIndex = 0;
    p2v_threadvar_desc *ptThrVarDes = &(P2V_GET_THRVAEDES_LCORE_PARA());
    while((rte_hash_iterate(h, &next_key, &next_data, &curIndex) >= 0) )
    {
        if (next_key != NULL)
        {       
            ptFlowCnt  = (p2v_flow_cnt *)next_data;

            /*1. 输出结果*/
            if (ptFlowCnt!=NULL)
            {
                p2vOutputEigenvalToBuf(ptFlowCnt);
                rte_free(ptFlowCnt);
            }
            /*2. 删除记录*/
            DeleteDbTblByKey(h, next_key);
            
            ptThrVarDes->flowRdDelCount++;
        }
    }
    return ;
}


int p2vWorkFlowStart(p2v_file_node *ptFileNode)
{
     p2v_threadvar_desc *ptThrVarDes = &(P2V_GET_THRVAEDES_LCORE_PARA());
    /*1. 调用pcap file 读取函数*/
    
    Packet_Handler pkt_handler;
    memset(&pkt_handler, 0, sizeof(pkt_handler));

    pkt_handler.file_name = ptFileNode->pcapFileName;
    printf("core %d :cur proc file %s\n",rte_lcore_id(),pkt_handler.file_name);
    pcap_t * ptPcapFile =  read_pcap(&pkt_handler);
    ptThrVarDes->ptInPcapName = ptFileNode->pcapFileName;
    /*2. 对读取的数据包并进行报文解析*/
    struct pcap_pkthdr tPcapPktHdr = {0};
    int totalPktNum = 0;
    struct rte_mbuf *pkts_burst[P2V_MAX_PKT_BURST] ;
    int ret = -1;
    do
    {   
        int pktNum = 0;
        /*1. 报文接收每次接收64个报文*/
        memset(pkts_burst, 0, P2V_MAX_PKT_BURST * sizeof(struct rte_mbuf *));
        ret = p2vRxburstFromPcap(ptPcapFile, pkts_burst,
                             ptThrVarDes->ptPktMbufMp, &pktNum);
        totalPktNum += pktNum;
        if (ret != 0)
        {
            /*1. 释放已分配的mbuf*/
            
            p2vFreeMbuf(pkts_burst, pktNum);
            break;
        }
        /*2.处理接收到的报文*/
        
        p2vPktProcess(pkts_burst, pktNum, ptThrVarDes->ptFlowTbl);
        p2vFreeMbuf(pkts_burst, pktNum);

    }while(pcap_next(ptPcapFile, &tPcapPktHdr)!=NULL);
    printf("core %d : cur file %s process %u pkts\n",rte_lcore_id(),pkt_handler.file_name, totalPktNum);
    ptThrVarDes->procPktsCount += totalPktNum;
    return ret;
}
/* main processing loop */
int
main_loop(__attribute__((unused)) void *dummy)
{

    
    p2v_global_mng *ptP2vGlbMng = (p2v_global_mng *)dummy;
    /*0. 为线程变量赋值*/
    p2v_threadvar_desc *ptThrVarDes = &(P2V_GET_THRVAEDES_LCORE_PARA());
    unsigned lcoreid = rte_lcore_id();
    unsigned socketid = rte_socket_id();
    
    ptThrVarDes->ptFlowTbl          = ptP2vGlbMng->tFlowTblMng.atThrFlowTbls[lcoreid].ptFlowTbl;
    ptThrVarDes->ptPcapFileMng      = &(ptP2vGlbMng->tWorkFlowMng.atPcapFileMng[lcoreid]);
    ptThrVarDes->ptPktMbufMp        = ptP2vGlbMng->tMpMng.atPktMbufMps[lcoreid].ptMp;
    ptThrVarDes->ptOutputEigenvalBuf= ptP2vGlbMng->tOutputBufMng.aptOutputEigenvalBuf[lcoreid];
    ptThrVarDes->flowTblAgeIndex    = 0;
    ptThrVarDes->flowTblAgeTime     = ptP2vGlbMng->ageTime;
    ptThrVarDes->ptOutputEigenvalBuf->ptOutFile = ptP2vGlbMng->ptoutput;
    ptThrVarDes->flowTblSz          = ptP2vGlbMng->flowTblSz;
    ptThrVarDes->ptFlwTblName       = ptP2vGlbMng->tFlowTblMng.atThrFlowTbls[lcoreid].flowTblName;

#if 0
    /*1. 打开输出文件.csv*/
    p2vSetOutputFileName(lcoreid, ptThrVarDes->ptOutputEigenvalBuf->outputCsvFileName, 
                        ptP2vGlbMng->outDirName);
    
    FILE *ptOutFile = GetCsvFileHandle(ptThrVarDes->ptOutputEigenvalBuf->outputCsvFileName);
    if (NULL == ptOutFile)
    {
        return 0;
    }
    ptThrVarDes->ptOutputEigenvalBuf->ptOutFile = ptOutFile;
#endif    
    /*2. workflow process*/
    p2v_file_node *ptFileNode = NULL;
    p2v_list_for_each_entry(ptFileNode, &(ptThrVarDes->ptPcapFileMng ->tPcapfileListHead), node)
    {
        p2vWorkFlowStart(ptFileNode);
    }
    /*3.本线程结束遍历流表输出结果*/
    
    p2vFlowTblClr(ptThrVarDes->ptFlowTbl);
    /*4.将缓存区内容输出到文件中，关闭文件*/
    p2v_output_eigenval_buf  *ptOutputEigenvalBuf = ptThrVarDes->ptOutputEigenvalBuf;
    int index = 0;
    if(ptOutputEigenvalBuf->outCount > 0)
    {
        for (index = 0; (index < P2V_OUTPUT_BUF_SIZE )
            && (index < ptOutputEigenvalBuf->outCount); index ++)
        {
            WriteFlowStatsInfoToCsvFile(ptOutputEigenvalBuf->ptOutFile, &(ptOutputEigenvalBuf->tEigenvals[index]));
        }
    }
    /*5. 打印本线程统计信息*/
    printf("coreid %u, flowRdCrtCount : %u\n",lcoreid, ptThrVarDes->flowRdCrtCount);
    printf("coreid %u, flowRdDelCount : %u\n",lcoreid, ptThrVarDes->flowRdDelCount);    
    printf("coreid %u, procPktsCount : %u\n",lcoreid, ptThrVarDes->procPktsCount);
    printf("coreid %u, parsePktsErrCount : %u\n",lcoreid, ptThrVarDes->parsePktsErrCount);
    printf("coreid %u, flwNrmDelCount : %u\n",lcoreid, ptThrVarDes->flwNrmDelCount);
    printf("coreid %u, flwTblCrtFailPkts : %u\n",lcoreid, ptThrVarDes->flwTblCrtFailPkts);
	return 0;
}


