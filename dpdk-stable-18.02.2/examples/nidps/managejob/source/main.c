/* SPDX-License-Identifier: BSD-3-Clause
 * Copyright(c) 2010-2016 Intel Corporation
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>
#include <sys/types.h>
#include <sys/queue.h>
#include <netinet/in.h>
#include <setjmp.h>
#include <stdarg.h>
#include <ctype.h>
#include <errno.h>
#include <getopt.h>
#include <signal.h>
#include <stdbool.h>

#include <sys/types.h>    
#include <sys/stat.h>  
#include <rte_tailq.h>

#include <rte_memory.h>
#include <rte_launch.h>
#include <rte_eal.h>
#include <rte_per_lcore.h>
#include <rte_lcore.h>
#include <rte_debug.h>



#include  "managejob.h"

#include  "rspackets.h"

#include "p2v_file.h"




/* display usage */
void p2vCmdlineUsage(const char *prgname)
{
	printf("%s [EAL options] -- -[pcap2vec options]\n"
	       "  -i input pcap dir name: all pcap files in dirs.\n"
	       "  -o output result dir name : all result files in dir.\n",
	       prgname);
}

unsigned int p2vParseFlowTblSz(const char *q_arg)
{
    char *end = NULL;
    unsigned long n;

    /* parse hexadecimal string */
    n = strtoul(q_arg, &end, 10);
    if ((q_arg[0] == '\0') || (end == NULL) || (*end != '\0'))
    	return 0;
    if (n == 0)
    	return 0;


	return n;
}

unsigned int p2vParseFlowTblAgeTime(const char *q_arg)
{
    char *end = NULL;
    unsigned long n;

    /* parse hexadecimal string */
    n = strtoul(q_arg, &end, 10);
    if ((q_arg[0] == '\0') || (end == NULL) || (*end != '\0'))
    	return 0;
    if (n == 0)
    	return 0;


	return n;
}


int p2vParseCmdline(int argc, char **argv, p2v_global_mng *ptP2vCfgParam)
{
	int cmd_opt;
	int option_index;
	char **argvopt;
    
	char *prgname = argv[0];
	static struct option lgopts[] = {
        {NULL, 0, 0, 0}
	};

    argvopt = argv;
    while ((cmd_opt = getopt_long(argc, argvopt, "i:o:s:g:", lgopts,
                &option_index)) != EOF) {
		switch (cmd_opt) {
		
		case 'i':
			/* 获取输入pcap文件所在得目录*/
            memcpy(ptP2vCfgParam->inDirName, optarg, P2V_NAME_LEN-1);
            if ('\0' != ptP2vCfgParam->inDirName[0])
            {
                printf("inDirName : %s\n", ptP2vCfgParam->inDirName);
            }
            else 
            {
                p2vCmdlineUsage(prgname);
            }
			break;
        case 'o':
            
            memcpy(ptP2vCfgParam->outFileName, optarg, P2V_NAME_LEN-1);
            if ('\0' != ptP2vCfgParam->outFileName[0])
            {
                printf("outDirName : %s\n", ptP2vCfgParam->outFileName);
            }
            else 
            {
                p2vCmdlineUsage(prgname);
            }
			break;
        case 'g':
            ptP2vCfgParam->ageTime = p2vParseFlowTblAgeTime(optarg);
            printf("agetime : %u\n", ptP2vCfgParam->ageTime);
            break;
        case 's':
            ptP2vCfgParam->flowTblSz = p2vParseFlowTblSz(optarg);
            printf("flowTblSz : %u\n", ptP2vCfgParam->flowTblSz);
            break;
		default:
			p2vCmdlineUsage(prgname);
			return -1;
		}
	}
	return 0;
}

int main(int argc, char **argv)
{   
    /*0.dpdk eal init*/
    int ret;
    unsigned lcore_id;

    ret = rte_eal_init(argc, argv);
    if (ret < 0)
    {
        rte_panic("Cannot init EAL\n");
    }
    argc -= ret;
    argv += ret;
    rte_log_set_global_level(RTE_LOG_DEBUG);

    /*1.0 解析argv参数，获取输入参数*/

    p2v_global_mng p2vGlbMng = {0};
	ret = p2vParseCmdline(argc, argv, &p2vGlbMng);
    if (P2V_FAIL == ret)
    {  
        rte_panic("Cannot correct parse arg!\n");
    }
    /*1.1 读取pcap文件目录下所有文件名*/
    
    ret = p2vGetPcapFileNameInDir(p2vGlbMng.inDirName, &(p2vGlbMng.tPcapFileMng ));
    if (P2V_FAIL == ret)
    {   /*1.0.1 遍历之前分配的文件节点，释放内存*/
        
        p2vFreeFileNode(&(p2vGlbMng.tPcapFileMng ));
        
        rte_panic("get pcapfile name fail!\n");
    }
    /*2.0 为每个线程建立收包队列所需内存*/
    ret = p2vCrtPktMbufPools(&(p2vGlbMng.tMpMng));
    if (P2V_FAIL == ret)
    {  
        p2vFreeFileNode(&(p2vGlbMng.tPcapFileMng ));
        p2vDelPktMbufPools(&(p2vGlbMng.tMpMng));
        rte_panic("alloc pktmbuf mp fail!\n");
    }
    /*2.1 为每个线程建立流表*/
    ret = p2vCrtFlowTbls(&(p2vGlbMng.tFlowTblMng), p2vGlbMng.flowTblSz);
    if (P2V_FAIL == ret)
    {  
        p2vFreeFileNode(&(p2vGlbMng.tPcapFileMng ));
        p2vDelPktMbufPools(&(p2vGlbMng.tMpMng));
        p2vDelFlowTbls(&(p2vGlbMng.tFlowTblMng));
        rte_panic("alloc flow tables  fail!\n");
    }

     /*2.2 为每个线程配置输出缓冲区*/
    
    ret = p2vCrtOutputBuf(&(p2vGlbMng.tOutputBufMng));
    
    if (P2V_FAIL == ret)
    {  
        p2vFreeFileNode(&(p2vGlbMng.tPcapFileMng ));
        p2vDelPktMbufPools(&(p2vGlbMng.tMpMng));
        p2vDelFlowTbls(&(p2vGlbMng.tFlowTblMng));
        
        p2vDelOutputBuf(&(p2vGlbMng.tOutputBufMng));
        rte_panic("alloc flow tables  fail!\n");
    }

    
    /*3.1 打开输出目录*/

    
    p2vGlbMng.ptoutput = GetCsvFileHandle(p2vGlbMng.outFileName);
    if (NULL == p2vGlbMng.ptoutput)
    {
        p2vFreeFileNode(&(p2vGlbMng.tPcapFileMng ));
        p2vDelPktMbufPools(&(p2vGlbMng.tMpMng));
        p2vDelFlowTbls(&(p2vGlbMng.tFlowTblMng));
        
        p2vDelOutputBuf(&(p2vGlbMng.tOutputBufMng));
        rte_panic("alloc flow tables  fail!\n");
    }
    /*3.0为每个数据包文件分配工作线程*/
    
    p2vAssignWorkFlow(&(p2vGlbMng.tWorkFlowMng),&(p2vGlbMng.tPcapFileMng ) );
	/*3.2启动各个工作线程*/
	rte_eal_mp_remote_launch(main_loop, (void *)&p2vGlbMng,  CALL_MASTER);//CALL_MASTER表示在master也会启动线程
	
	RTE_LCORE_FOREACH_SLAVE(lcore_id)
	{ //遍历每个slave lcore
	    if (rte_eal_wait_lcore(lcore_id) < 0) //等待线程结束
	    {
	    	return -1;
	    }
	}

	rte_eal_mp_wait_lcore();
    
    p2vDelWorkFlow(&(p2vGlbMng.tWorkFlowMng));
    p2vDelPktMbufPools(&(p2vGlbMng.tMpMng));
    p2vDelFlowTbls(&(p2vGlbMng.tFlowTblMng));
    
    p2vDelOutputBuf(&(p2vGlbMng.tOutputBufMng));
    
    CloseCsvFile(p2vGlbMng.ptoutput);
	return 0;
}
