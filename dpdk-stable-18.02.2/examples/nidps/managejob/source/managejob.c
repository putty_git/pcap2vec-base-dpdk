#include "managejob.h"
#include "rte_malloc.h"

#define PKTMBUF_MEMPOOL_NAME ("PKTMBUF_MEMPOOL_%u_%u")

#define FLOWTBL_NAME ("FLOWTBL_NAME_%u_%u")

void p2vSetMpName(u32 socketId, u32 lcoreId, char *ptRingName)
{

    snprintf(ptRingName, (P2V_NAME_LEN - 1), PKTMBUF_MEMPOOL_NAME, socketId, lcoreId);
    return ;
}

int  p2vCrtPktMbufPools(p2v_mp_mng *ptMpMng)
{
    u32 mbufCfgIndex = 0;
    p2v_pktmbuf_mp* ptPktMbufPool = ptMpMng->atPktMbufMps;
    for (mbufCfgIndex = 0; mbufCfgIndex < P2V_MAX_THREAD_NUM; mbufCfgIndex ++)
    {
        /*1. 判断当前cpu是否配置*/
        if (rte_lcore_is_enabled(mbufCfgIndex) != 1 )
        {
            continue;
        }
        p2v_pktmbuf_mp* ptTmpMp = ptPktMbufPool + mbufCfgIndex;
        ptTmpMp->bufferSize = P2V_MP_BUFSIZE;
        ptTmpMp->poolSize   = P2V_MP_RINGDEPTH;
        ptTmpMp->cacheSize  = P2V_MP_CACHESIZE;
        ptTmpMp->privateSize= P2V_MP_PIVSIZE;
        ptTmpMp->socketId   = eal_cpu_socket_id(mbufCfgIndex);
        p2vSetMpName(ptTmpMp->socketId, mbufCfgIndex, ptTmpMp->mempoolName);
        ptTmpMp->ptMp = rte_pktmbuf_pool_create(ptTmpMp->mempoolName, ptTmpMp->poolSize,
                            ptTmpMp->cacheSize, ptTmpMp->privateSize,
                            ptTmpMp->bufferSize, ptTmpMp->socketId);
        if(NULL == ptTmpMp->ptMp)
        {
            printf("alloc mp fail!\n");
            return P2V_FAIL;
            
        }
    }
    
    return P2V_SUCCESS;
}

void p2vDelPktMbufPools(p2v_mp_mng *ptMpMng)
{
    u32 mbufCfgIndex = 0;
    p2v_pktmbuf_mp* ptPktMbufPool = ptMpMng->atPktMbufMps;
    for (mbufCfgIndex = 0; mbufCfgIndex < P2V_MAX_THREAD_NUM; mbufCfgIndex ++)
    {
        /*1. 判断当前cpu是否配置*/
        if(ptPktMbufPool[mbufCfgIndex].ptMp != NULL)
        {
            rte_mempool_free(ptPktMbufPool[mbufCfgIndex].ptMp);
        }
    }
    
    return ;
}



u32 p2vCrtSingleDbTable(p2v_thr_flow_tbl *ptThrFlowTbl)
{   
    struct rte_hash_parameters tparams = {0};
    tparams.name = ptThrFlowTbl->flowTblName;
    tparams.entries = ptThrFlowTbl->capacity;
    tparams.key_len = sizeof(FIVE_TUPLE_KEY);
    tparams.socket_id = ptThrFlowTbl->socketid;

    u32 ret = P2V_FAIL;
    ptThrFlowTbl->ptFlowTbl = CreateDbTbl(&tparams);
    if (NULL != ptThrFlowTbl->ptFlowTbl)
    {    
        ret = P2V_SUCCESS;        
    }
    return ret;
}


void p2vSetFlowTblName(u32 socketId, u32 lcoreId, char *ptFlowTblName)
{

    snprintf(ptFlowTblName, (P2V_NAME_LEN - 1), FLOWTBL_NAME, socketId, lcoreId);
    return ;
}


u32 p2vCrtFlowTbls(p2v_flow_tbl_mng *ptFlowTblMng, u32 flowTblSz)
{
    u32 tbIndex;
    u32 ret = P2V_FAIL;
    p2v_thr_flow_tbl *ptThrFlowTbl = ptFlowTblMng->atThrFlowTbls;
    for (tbIndex = 0; tbIndex < P2V_MAX_THREAD_NUM; tbIndex ++)
    {
        
       /*1. 判断当前cpu是否配置*/
        if (rte_lcore_is_enabled(tbIndex) != 1 )
        {
            continue;
        }
        p2v_thr_flow_tbl *ptTmpFlowTbl = ptThrFlowTbl + tbIndex;
        ptTmpFlowTbl->capacity = flowTblSz;
        ptTmpFlowTbl->socketid = eal_cpu_socket_id(tbIndex);
        p2vSetFlowTblName(ptTmpFlowTbl->socketid, tbIndex, ptTmpFlowTbl->flowTblName); 
        ret = p2vCrtSingleDbTable(ptTmpFlowTbl);
        if (ret!=P2V_SUCCESS)
        {
            break;            
        }
    }
    return ret;
}

void p2vDelFlowTbls(p2v_flow_tbl_mng *ptFlowTblMng)
{
    u32 tbIndex;
    p2v_thr_flow_tbl *ptThrFlowTbl = ptFlowTblMng->atThrFlowTbls;
    for (tbIndex = 0; tbIndex < P2V_MAX_THREAD_NUM; tbIndex ++)
    {
        
        if (ptThrFlowTbl[tbIndex].ptFlowTbl != NULL)
        {
            DestroyDbTbl(ptThrFlowTbl[tbIndex].ptFlowTbl);
        }
    }
    delDbTblRegisterArray();
    return ;
}


int p2vCrtOutputBuf(p2v_output_buf_mng *ptOutputBufMng)
{
    u32 index;
    u32 ret = P2V_FAIL;
    p2v_output_eigenval_buf **ptOutputBuf = ptOutputBufMng->aptOutputEigenvalBuf;
    
     for (index = 0; index < P2V_MAX_THREAD_NUM; index ++)
    {
        
       /*1. 判断当前cpu是否配置*/
        if (rte_lcore_is_enabled(index) != 1 )
        {
            continue;
        }
        u32 socketid = eal_cpu_socket_id(index);
        ptOutputBuf[index] = rte_zmalloc_socket("p2v_output_eigenval_buf", sizeof(p2v_output_eigenval_buf), RTE_CACHE_LINE_SIZE, socketid);
        if (NULL == ptOutputBuf[index])
        {
            return P2V_FAIL;
        }
    }
    return P2V_SUCCESS;
}

void p2vDelOutputBuf(p2v_output_buf_mng *ptOutputBufMng)
{
    u32 index;
    u32 ret = P2V_FAIL;
    p2v_output_eigenval_buf **ptOutputBuf = ptOutputBufMng->aptOutputEigenvalBuf;
    
    for (index = 0; index < P2V_MAX_THREAD_NUM; index ++)
    {
        if (NULL != ptOutputBuf[index])
        {
            rte_free(ptOutputBuf[index]);
        }
    }
    return;
}

void p2vAssignWorkFlow(p2v_workflow_mng *ptP2vWorkFlowMng, p2v_pcapfile_mng *ptSrcTotalPFMng)
{
    unsigned int ilcoreId;

    int enCpuMap[P2V_MAX_THREAD_NUM] ;
    int enCpuNum = 0;
    memset(enCpuMap, 0xff, P2V_MAX_THREAD_NUM*sizeof(int));
    p2v_pcapfile_mng *ptPcapFileMng = ptP2vWorkFlowMng->atPcapFileMng;
    for(ilcoreId = 0; ilcoreId < P2V_MAX_THREAD_NUM; ilcoreId ++)
    {
        /*1. 判断当前cpu是否配置*/
        if (rte_lcore_is_enabled(ilcoreId) != 1 )
        {
            continue;
        }
        enCpuMap[enCpuNum++] = ilcoreId;
        p2v_pcapfile_mng *ptTmp = ptPcapFileMng + ilcoreId;
        P2V_INIT_LIST_HEAD((&(ptTmp->tPcapfileListHead)));
    }
    int threshold = ptSrcTotalPFMng->pcapFileNum / enCpuNum + 10;
    p2v_file_node *ptPcapFile = NULL;
    p2v_file_node *ptNext= NULL;
    const int seed = 100;
    p2v_list_for_each_entry_safe(ptPcapFile, ptNext, &(ptSrcTotalPFMng->tPcapfileListHead), node)
    {
        int len = strnlen(ptPcapFile->pcapFileName , P2V_NAME_LEN);
        

        unsigned int hashval =  p2vMurMurHash(ptPcapFile->pcapFileName, len, seed);
        int lcoreIndex = hashval % enCpuNum;
        
        p2v_pcapfile_mng *ptTmp = ptPcapFileMng + enCpuMap[lcoreIndex];
        if(ptTmp->pcapFileNum >= threshold)
        {
            unsigned int hashval =  p2vMurMurHash(ptPcapFile->pcapFileName, len, (seed + 1));
            lcoreIndex = hashval % enCpuNum;
            ptTmp = ptPcapFileMng + enCpuMap[lcoreIndex];
        }
        
        p2v_list_del(ptPcapFile);
        p2v_list_add((&(ptPcapFile->node)), &(ptTmp->tPcapfileListHead));
        ptTmp->pcapFileNum++;
        
    }
    return;
}



void p2vDelWorkFlow(p2v_workflow_mng *ptP2vWorkFlowMng)
{
    unsigned int ilcoreId;


    p2v_pcapfile_mng *ptPcapFileMng = ptP2vWorkFlowMng->atPcapFileMng;
    for (ilcoreId = 0; ilcoreId < P2V_MAX_THREAD_NUM; ilcoreId ++)
    {
        if (ptPcapFileMng[ilcoreId].pcapFileNum!=0)
        {
            p2vFreeFileNode(&(ptPcapFileMng[ilcoreId]));
        }
        
    }
    return;
}

