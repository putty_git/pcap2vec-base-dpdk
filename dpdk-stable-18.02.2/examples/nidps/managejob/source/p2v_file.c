#include "p2v_file.h"
#include "rte_malloc.h"


int p2vGetPcapFileNameInDir(char *pInDir, p2v_pcapfile_mng* ptPcapFileMng )
{
    DIR *dir = NULL;
    struct dirent *ptr = NULL;
    
    int lcoreId = rte_lcore_id();
    int socketId = rte_lcore_to_socket_id(lcoreId);
    P2V_INIT_LIST_HEAD(&ptPcapFileMng->tPcapfileListHead);
 
    if ((dir=opendir(pInDir)) == NULL)
    {
        printf("Cannot open input dir :%s!\n", pInDir);
        return P2V_FAIL;
    }
    while ((ptr=readdir(dir)) != NULL)
    {
        if(strcmp(ptr->d_name,".")==0 || strcmp(ptr->d_name,"..")==0)    ///current dir OR parrent dir
        {
            continue;
        }
        if(ptr->d_type != 8)    ///file
        {
            continue;
        }
        /*1.1分配文件节点内存*/
        
//        p2v_file_node *ptFileNode = rte_malloc("p2v_file_node", sizeof(p2v_file_node), RTE_CACHE_LINE_SIZE);
        p2v_file_node *ptFileNode = rte_zmalloc_socket("p2v_file_node", sizeof(p2v_file_node), RTE_CACHE_LINE_SIZE, socketId);
        if (NULL == ptFileNode)
        {
            printf("cur file %s , memory no enough !\n", ptr->d_name);
            return P2V_FAIL;
        }
        unsigned dirNameLen     = strnlen(pInDir, P2V_NAME_LEN-1);
        unsigned fileNameLen    = strnlen(ptr->d_name, P2V_NAME_LEN-1);
        if (dirNameLen +  fileNameLen > P2V_NAME_LEN)
        {
            printf("cur file %s name too long!\n", ptr->d_name);
            continue;
        }
        memcpy(ptFileNode->pcapFileName, pInDir, dirNameLen);
        //strcat(ptFileNode->pcapFileName + dirNameLen, ptr->d_name);
        memcpy(ptFileNode->pcapFileName+ dirNameLen ,ptr->d_name, P2V_NAME_LEN-1);

        
    
        /*1.2 添加到File MNG*/
    
        p2v_list_add((&(ptFileNode->node)), &(ptPcapFileMng->tPcapfileListHead));
        ptPcapFileMng->pcapFileNum++;
    }
    closedir(dir);

}

void p2vFreeFileNode(p2v_pcapfile_mng* ptPcapFileMng)
{
    
    p2v_file_node *ptPcapFile = NULL;
    p2v_file_node *ptNext = NULL;
    p2v_list_for_each_entry_safe(ptPcapFile, ptNext, &(ptPcapFileMng->tPcapfileListHead), node)
    {
        p2v_list_del(ptPcapFile);
        rte_free(ptPcapFile);
    }
    return;
}

void p2vSetOutputFileName(int lcoreid, char *outfilename, char* dirName)
{    
    uint32_t  oDirNameLen =  strnlen(dirName, P2V_NAME_LEN-1);

    memcpy(outfilename, dirName, oDirNameLen);

    snprintf(outfilename + oDirNameLen, (P2V_NAME_LEN - oDirNameLen - 1), "core_%u.csv", lcoreid);
}
//csv文件标题行的内容
static const char *csv_file_title = 
"flow_duration(s),\
total_fwd_packets,\
total_bwd_packets,\
total_length_of_fwd_packet,\
total_length_of_bwd_packet,\
fwd_packet_length_min,\
fwd_packet_length_max,\
fwd_packet_length_mean,\
fwd_packet_length_std,\
bwd_packet_length_min,\
bwd_packet_length_max,\
bwd_packet_length_mean,\
bwd_packet_length_std,\
flow_bytes_per_second,\
flow_packets_per_second,\
flow_iat_mean,\
flow_iat_std,\
flow_iat_max,\
flow_iat_min,\
fwd_iat_min,\
fwd_iat_max,\
fwd_iat_mean,\
fwd_iat_std,\
fwd_iat_total,\
bwd_iat_min,\
bwd_iat_max,\
bwd_iat_mean,\
bwd_iat_std,\
bwd_iat_total,\
fwd_psh_flags,\
bwd_psh_flags,\
fwd_urg_flags,\
bwd_urg_flags,\
fwd_header_length,\
bwd_header_length,\
fwd_packets_per_second,\
bwd_packets_per_second,\
packet_length_min,\
packet_length_max,\
packet_length_mean,\
packet_length_std,\
packet_length_variance,\
fin_flag_count,\
syn_flag_count,\
rst_flag_count,\
psh_flag_count,\
ack_flag_count,\
urg_flag_count,\
cwr_flag_count,\
ece_flag_count,\
down_vs_up_ratio,\
average_packet_size,\
fwd_segment_size_avg,\
bwd_segment_size_avg,\
fwd_init_win_bytes,\
bwd_init_win_bytes,\
fwd_act_data_pkts,\
fwd_seg_size_min,\
active_min,\
active_mean,\
active_max,\
active_std,\
idle_min,\
idle_mean,\
idle_max,\
idle_std,\
pcapfilename\n";

//根据文件名称创建csv文件,并返回文件指针
FILE *GetCsvFileHandle(const char *csv_file_name)
{
	if(NULL == csv_file_name)
	{
		printf("[%s][%s][line %d] csv_file_name is NULL!\n",__FILE__,__func__,__LINE__);
		return NULL;
	}

	//打开文件
	FILE *fp = NULL;
	fp = fopen(csv_file_name, "a");
	if(NULL == fp)
	{
		printf("[%s][%s][line %d] open file[%s] failed!\n",__FILE__,__func__,__LINE__,csv_file_name);
		return NULL;
	}
	
	//格式化写入csv文件的标题(只在创建文件的时候,写入一次即可)
	fprintf(fp,csv_file_title);
	
	//刷新写入缓冲区
	fflush(fp); 
	
	return fp;
}




//以追加方式将统计特征写入csv文件中,格式化写入
int WriteFlowStatsInfoToCsvFile(FILE *fp, p2v_eigenval *output_data)
{
	if(NULL == fp || NULL == output_data)
	{
		printf("[%s][%s][line %d] fp or output_data is NULL!\n",__FILE__,__func__,__LINE__);
		return -1;
	}
	
//格式化写入
fprintf(fp,\
"\
%lf,\
%d,\
%d,\
%d,\
%d,\
%d,\
%d,\
%d,\
%d,\
%d,\
%d,\
%d,\
%d,\
%d,\
%d,\
%lf,\
%lf,\
%lf,\
%lf,\
%lf,\
%lf,\
%lf,\
%lf,\
%lf,\
%lf,\
%lf,\
%lf,\
%lf,\
%lf,\
%d,\
%d,\
%d,\
%d,\
%d,\
%d,\
%d,\
%d,\
%d,\
%d,\
%d,\
%d,\
%d,\
%d,\
%d,\
%d,\
%d,\
%d,\
%d,\
%d,\
%d,\
%lf,\
%d,\
%d,\
%d,\
%d,\
%d,\
%d,\
%d,\
%lf,\
%lf,\
%lf,\
%lf,\
%lf,\
%lf,\
%lf,\
%lf,\
%s\n",\
output_data->flow_duration,\
output_data->total_fwd_packets,\
output_data->total_bwd_packets,\
output_data->total_length_of_fwd_packet,\
output_data->total_length_of_bwd_packet,\
output_data->fwd_packet_length_min,\
output_data->fwd_packet_length_max,\
output_data->fwd_packet_length_mean,\
output_data->fwd_packet_length_std,\
output_data->bwd_packet_length_min,\
output_data->bwd_packet_length_max,\
output_data->bwd_packet_length_mean,\
output_data->bwd_packet_length_std,\
output_data->flow_bytes_per_second,\
output_data->flow_packets_per_second,\
output_data->flow_iat_mean,\
output_data->flow_iat_std,\
output_data->flow_iat_max,\
output_data->flow_iat_min,\
output_data->fwd_iat_min,\
output_data->fwd_iat_max,\
output_data->fwd_iat_mean,\
output_data->fwd_iat_std,\
output_data->fwd_iat_total,\
output_data->bwd_iat_min,\
output_data->bwd_iat_max,\
output_data->bwd_iat_mean,\
output_data->bwd_iat_std,\
output_data->bwd_iat_total,\
output_data->fwd_psh_flags,\
output_data->bwd_psh_flags,\
output_data->fwd_urg_flags,\
output_data->bwd_urg_flags,\
output_data->fwd_header_length,\
output_data->bwd_header_length,\
output_data->fwd_packets_per_second,\
output_data->bwd_packets_per_second,\
output_data->packet_length_min,\
output_data->packet_length_max,\
output_data->packet_length_mean,\
output_data->packet_length_std,\
output_data->packet_length_variance,\
output_data->fin_flag_count,\
output_data->syn_flag_count,\
output_data->rst_flag_count,\
output_data->psh_flag_count,\
output_data->ack_flag_count,\
output_data->urg_flag_count,\
output_data->cwr_flag_count,\
output_data->ece_flag_count,\
output_data->down_vs_up_ratio,\
output_data->average_packet_size,\
output_data->fwd_segment_size_avg,\
output_data->bwd_segment_size_avg,\
/*
output_data->fwd_bytes_bulk_avg,\
output_data->fwd_packet_bulk_avg,\
output_data->fwd_bulk_rate_avg,\
output_data->bwd_bytes_bulk_avg,\
output_data->bwd_packet_bulk_avg,\
output_data->bwd_bulk_rate_avg,\
output_data->subflow_fwd_packets,\
output_data->subflow_fwd_bytes,\
output_data->subflow_bwd_packets,\
output_data->subflow_bwd_bytes,\
*/
output_data->fwd_init_win_bytes,\
output_data->bwd_init_win_bytes,\
output_data->fwd_act_data_pkts,\
output_data->fwd_seg_size_min,\
output_data->active_min,\
output_data->active_mean,\
output_data->active_max,\
output_data->active_std,\
output_data->idle_min,\
output_data->idle_mean,\
output_data->idle_max,\
output_data->idle_std, \
output_data->outputPcapFileName
);
	
	//刷新写入缓冲区
	fflush(fp); 
	
	return 0;
}


//关闭文件
int CloseCsvFile(FILE *fp)
{
	if(NULL != fp)
	{
		fclose(fp);
		fp = NULL;
	}
	
	return 0;
}


