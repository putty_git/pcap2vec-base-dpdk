#ifndef _P2V_FLOW_H_
#define _P2V_FLOW_H_
#include "protocol_common.h"
#include <sys/time.h>


#define P2V_NAME_LEN            (128)
typedef struct rte_flow_close_status
{
    uint8_t  fwdClsStatus;
    uint8_t  bwdClsStatus;
    uint8_t  tcpClsStatus;
    uint8_t  rsv;
}p2v_flow_close_status;
typedef struct rte_flow_table_data
{
    LINK_STATS_INFO tLinkStatsInfo;
    uint32_t fwdPktCount;
    uint32_t bwdPKtCount;
    p2v_flow_close_status tFlowClsStus;
    char     flowinPcapName[P2V_NAME_LEN];
    struct timeval   crtTime;
    
}p2v_flow_cnt;

enum FLOWDIR
{
    FWD_DIR,
    BWD_DIR,
};

#endif