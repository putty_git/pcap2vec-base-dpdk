#ifndef _P2V_OUTPUT_H_
#define _P2V_OUTPUT_H_



//约定输出的字段结构定义(详见约定文档字段内容及字段顺序)
typedef struct Convert_Stats_Info_To_Csv
{
    double          flow_duration;   //64

    unsigned int  total_fwd_packets;
    unsigned int  total_bwd_packets;//64
    
    unsigned int  total_length_of_fwd_packet;
    unsigned int  total_length_of_bwd_packet;//64
    
    unsigned int  fwd_packet_length_min;
    unsigned int  fwd_packet_length_max;//64
    
    unsigned int  fwd_packet_length_mean;
    int           fwd_packet_length_std;//64
    
    unsigned int  bwd_packet_length_min;
    unsigned int  bwd_packet_length_max;
    
    unsigned int  bwd_packet_length_mean;
    int           bwd_packet_length_std;
    
    unsigned int  flow_bytes_per_second;
    unsigned int  flow_packets_per_second;
    
    double         flow_iat_mean;
    double         flow_iat_std;
    
    double         flow_iat_max;
    double	      flow_iat_min;
    
    double	      fwd_iat_min;
    double	      fwd_iat_max;
    
    double	      fwd_iat_mean;
    double	      fwd_iat_std;
    
    double	      fwd_iat_total;
    double	      bwd_iat_min;
    
    double	      bwd_iat_max;
    double	      bwd_iat_mean;
    
    double	      bwd_iat_std;
    double	      bwd_iat_total;
    
    unsigned int  fwd_psh_flags;
    unsigned int  bwd_psh_flags;
    
    unsigned int  fwd_urg_flags;
    unsigned int  bwd_urg_flags;
    
    unsigned int  fwd_header_length;
    unsigned int  bwd_header_length;
    
    unsigned int  fwd_packets_per_second;
    unsigned int  bwd_packets_per_second;
    
    unsigned int  packet_length_min;
    unsigned int  packet_length_max;
    
    unsigned int  packet_length_mean;
    unsigned int  packet_length_std;
    
    unsigned int  packet_length_variance;
    unsigned int  fin_flag_count;
    
    unsigned int  syn_flag_count;
    unsigned int  rst_flag_count;
    
    unsigned int  psh_flag_count;
    unsigned int  ack_flag_count;
    
    unsigned int  urg_flag_count;
    unsigned int  cwr_flag_count;
    
    unsigned int  ece_flag_count;   
    unsigned int  average_packet_size;
    
    unsigned int  fwd_segment_size_avg;  
    unsigned int  bwd_segment_size_avg;

    
    double         down_vs_up_ratio;
    #if 0
	unsigned int  fwd_bytes_bulk_avg;
	unsigned int  fwd_packet_bulk_avg;
	unsigned int  fwd_bulk_rate_avg;
	unsigned int  bwd_bytes_bulk_avg;
	unsigned int  bwd_packet_bulk_avg;
	unsigned int  bwd_bulk_rate_avg;
	unsigned int  subflow_fwd_packets;
	unsigned int  subflow_fwd_bytes;
	unsigned int  subflow_bwd_packets;
	unsigned int  subflow_bwd_bytes;
   #endif
    unsigned int  fwd_init_win_bytes;
    unsigned int  bwd_init_win_bytes;
    
    unsigned int  fwd_act_data_pkts;
    unsigned int  fwd_seg_size_min;
    
    double	      active_min;    
    double	      active_mean;    
    double	      active_max;    
    double	      active_std;
    double	      idle_min;
    double	      idle_mean;
    double	      idle_max;
    double	      idle_std;

    
    char outputPcapFileName[P2V_NAME_LEN];
}CONVERT_STATS_INFO_TO_CSV;

typedef CONVERT_STATS_INFO_TO_CSV p2v_eigenval;

#endif
