#ifndef MANAGEJOB_H_
#define MANAGEJOB_H_
/*---------------head file start--------------------------------------*/
#include  "macro.h"
#include "p2v_flow.h"
#include "nidps_db.h"
#include "nidps_list.h"
#include "p2v_output.h"
/*---------------head file end----------------------------------------*/
/*-----------------------dpdk head file start--------------------*/
#include "rte_hash.h"
#include "rte_mbuf.h"
#include "rte_lcore.h"

/*-----------------------dpdk head file end--------------------*/

/*---------------struct data start--------------------------------------*/
#define P2V_NAME_LEN            (128)
#define P2V_SUCCESS             (0)
#define P2V_FAIL                (-1)

#define P2V_MAX_THREAD_NUM      (32)

#define P2V_FLOWTBL_CAPACITY    (10240)

#define P2V_OUTPUT_BUF_SIZE     (128)

typedef enum rte_mp_param
{
    P2V_MP_BUFSIZE   = 2048,
    P2V_MP_RINGDEPTH = 1024,
    P2V_MP_CACHESIZE = 256,
    P2V_MP_PIVSIZE   = 0,
}p2v_en_mp_param;

typedef struct ret_pcapfile_mng
{
    struct p2v_list_head tPcapfileListHead;
    u32 pcapFileNum;
    u32 rsv;
}p2v_pcapfile_mng;

typedef struct rte_pktmbuf_mempool
{
    char mempoolName[P2V_NAME_LEN];
    struct rte_mempool *ptMp;
    u32 socketId;
    u32 poolSize;
    u32 cacheSize;
    u32 privateSize;
    u32 bufferSize;
    u32 freeNum;
    u32 useNum;
    u32 rsv;
    //Add mempool params;
}p2v_pktmbuf_mp;

typedef struct rte_mp_mng
{
    p2v_pktmbuf_mp atPktMbufMps[P2V_MAX_THREAD_NUM];
}p2v_mp_mng;

/*db-table:
  - flowtb:
    - capacity      : 1000000   #表容量记录数
    - is_threadtb   : true      #是否为线程表
    - thread_num    : 1         #如果为线程表，那么线程
    - core_socket   : (1,1)     #所在(cpu,numa)
*/
typedef struct rte_thr_flow_tbl
{
    struct rte_hash * ptFlowTbl;
    char flowTblName[P2V_NAME_LEN];  
    u32  capacity;
    u16  socketid;
    u16  rsv;
}p2v_thr_flow_tbl;

typedef struct rte_flow_tbl_mng
{
    p2v_thr_flow_tbl atThrFlowTbls[P2V_MAX_THREAD_NUM];
}p2v_flow_tbl_mng;
typedef struct  rte_workflow_mng
{
    p2v_pcapfile_mng atPcapFileMng[P2V_MAX_THREAD_NUM];

}p2v_workflow_mng;

typedef struct rte_output_eigenval_buf
{
    p2v_eigenval tEigenvals[P2V_OUTPUT_BUF_SIZE];
    FILE * ptOutFile;
    int outCount;
}p2v_output_eigenval_buf;

typedef struct rte_output_buf_mng
{
    p2v_output_eigenval_buf* aptOutputEigenvalBuf[P2V_MAX_THREAD_NUM];
}p2v_output_buf_mng;

typedef struct rte_global_mng
{
    char inDirName[P2V_NAME_LEN];
    char outFileName[P2V_NAME_LEN];
    u32  flowTblSz;
    u32   ageTime;
    FILE*            ptoutput;
    p2v_pcapfile_mng tPcapFileMng;
    p2v_mp_mng       tMpMng;
    p2v_flow_tbl_mng tFlowTblMng;
    p2v_workflow_mng tWorkFlowMng;
    p2v_output_buf_mng tOutputBufMng;
}p2v_global_mng;


typedef struct ret_file_node{
    struct p2v_list_head node;
    char pcapFileName[P2V_NAME_LEN];
} p2v_file_node;



void p2vCmdlineUsage(const char *prgname);
int p2vParseCmdline(int argc, char **argv, p2v_global_mng *ptP2vCfgParam);
int  p2vCrtPktMbufPools(p2v_mp_mng *ptMpMng);
u32 p2vCrtFlowTbls(p2v_flow_tbl_mng *ptFlowTblMng, u32 flowTblSz);
void p2vAssignWorkFlow(p2v_workflow_mng *ptP2vWorkFlowMng, p2v_pcapfile_mng *ptSrcTotalPFMng);

void p2vDelPktMbufPools(p2v_mp_mng *ptMpMng);
void p2vDelWorkFlow(p2v_workflow_mng *ptP2vWorkFlowMng);

int p2vCrtOutputBuf(p2v_output_buf_mng *ptOutputBufMng);

void p2vDelOutputBuf(p2v_output_buf_mng *ptOutputBufMng);


#endif
