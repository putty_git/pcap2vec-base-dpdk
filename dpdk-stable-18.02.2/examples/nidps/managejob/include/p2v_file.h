#ifndef P2V_FILE_H_
#define P2V_FILE_H_

#include <unistd.h>
#include <dirent.h>
#include<fcntl.h>

#include "managejob.h"


int p2vGetPcapFileNameInDir(char *pInDir, p2v_pcapfile_mng* ptPcapFileMng );
void p2vFreeFileNode(p2v_pcapfile_mng* ptPcapFileMng);
FILE *GetCsvFileHandle(const char *csv_file_name);
void p2vSetOutputFileName(int lcoreid, char *outfilename, char* dirName);
int WriteFlowStatsInfoToCsvFile(FILE *fp, p2v_eigenval *output_data);


//�ر��ļ�
int CloseCsvFile(FILE *fp);


#endif  
