#ifndef __PCAP_READER_H__
#define __PCAP_READER_H__


#include <stdlib.h>

#include <pcap.h>


typedef struct Packet_Handler_
{
	const char *file_name;
	pcap_handler pcap_handler_fuc;
	void *p_user_space;
	
}Packet_Handler;

void print_data(const uint8_t *data, int length);


pcap_t * read_pcap(void *pkt_handler_struct);



#endif
