
#include "pcap_reader.h"


/** datalink */
#define DLT_EN10MB 		1
#define DLT_RAW 		12





void print_data(const uint8_t *data, int length)
{
    int i, x, j, c;
    int w = 0;

    for( i=0; length>0; length -= 16 )
    {
        c = length >= 16 ? 16 : length;
        printf("%06X  ", w);
        w+=16;

        for( j=0; j<c; j++ )
            printf("%2.02X ", data[i+j]);

        for( x = length; x<16; x++ )
            printf("   ");

        for( j=0; j<c; j++ )
            printf("%c", (isprint(data[i+j]) ? data[i+j] : '.'));

        printf("\n");
        i+=c;
    }
}


pcap_t * read_pcap(void *pkt_handler_struct)
{
    Packet_Handler *pkt_handler = (Packet_Handler *)pkt_handler_struct;
    if (pkt_handler == NULL)
    {
        printf("pkt_handler is null\n");
        exit(EXIT_FAILURE);
    }
    
    const char* pcapfile = pkt_handler->file_name;	
    if (pcapfile == NULL)
    {
        printf("pcapfile is null\n");
        exit(EXIT_FAILURE);
    }

    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t  *pcap = NULL;
    pcap = pcap_open_offline(pcapfile, errbuf);
    if (!pcap)
	{
		printf("open error:%s (%s)\n", pcapfile, errbuf);
		return -1;
	}
            
	unsigned datalink = pcap_datalink(pcap);
	switch(datalink)
    {
        case DLT_EN10MB:
        case DLT_RAW:
			break;
		
		default:
			printf("datalink type is not supported (%u)", pcap_datalink(pcap));
			return -1;
    }
#if 0
	if (pcap_loop(pcap, -1, pkt_handler->pcap_handler_fuc, pkt_handler->p_user_space) == -1)
	{
		printf("%s", pcap_geterr(pcap));
		exit(EXIT_FAILURE);
	}

    pcap_close(pcap);
#endif
    return pcap;	
}



