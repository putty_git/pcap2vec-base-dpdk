/***************************************************************************************************
@file name:    nidps_db.h
@description:  define the data struct of nidps_db
@author:       wangkeyue
@time:         2021-03-16
@version:      V1.0
***************************************************************************************************/


#ifndef NIDPS_DB_H
#define NIDPS_DB_H


#include <stdio.h>
#include <stdlib.h>
#include <string.h>




#include "rte_hash.h"
#include <rte_fbk_hash.h>
#include <rte_jhash.h>
#include <rte_hash_crc.h>




#ifndef MAX_DB_TBL_NUM
#define MAX_DB_TBL_NUM 128
#endif


#define P2V_NAME_LEN 128


//根据函数返回值为有符号型和无符号型,分别定义成功与失败的宏
#define NIDPS_FUNC_RET_SINGED_SUCCESS   0
#define NIDPS_FUNC_RET_SINGED_FAILED   -1
#define NIDPS_FUNC_RET_UNSINGED_SUCCESS 1
#define NIDPS_FUNC_RET_UNSINGED_FAILED  0



//通过枚举类型,维护数据库表在g_DbTableNameArray中的下标索引
struct nidps_db_name_and_object
{
    char dbtblname[P2V_NAME_LEN];      //数据表的名称
    struct rte_hash *dbtblptr;  //数据表创建成功之后返回的对象指针
} __rte_cache_aligned;



//函数原型
/************************************************************************
@函数名称:      RegisterDbTblByName
@功能描述:      通过数据表名称注册数据表
@输入参数:      params---内存数据表的参数
@输出参数:      无
@返回值:       成功:返回数据表名称的索引; 失败:返回-1
@作者:        wangkeyue
@版本:        V1.0 创建
************************************************************************/
static int RegisterDbTblByName(const struct rte_hash_parameters *params);



/*****************************************************************************
@函数名称:      CreateDbTbl
@功能描述:      根据指定名称,创建内存数据表
@输入参数:      params---创建内存数据表的参数
@输出参数:      无
@返回值:       成功:返回数据表; 失败:退出程序
@作者:        wangkeyue
@版本:        V1.0 创建
*****************************************************************************/
struct rte_hash *CreateDbTbl(const struct rte_hash_parameters *params);



/*********************************************************************************
@函数名称:      InsertOrUpdateDbTblByKey
@功能描述:      根据指定key,向内存数据表插入data,如果key已经存在,则更新key相关联的data
@输入参数:      htbl---内存数据表; key---待插入记录的key; data---待插入记录的data
@输出参数:      无
@返回值:       成功:返回0; 失败:返回值为负整数
@作者:        wangkeyue
@版本:        V1.0 创建
*********************************************************************************/
int InsertDbTblByKey(const struct rte_hash *htbl, const void *key, void *data);



/*********************************************************************************
@函数名称:      SelectDbTblByKey
@功能描述:      根据指定key,在内存数据表查询与key相关的记录
@输入参数:      htbl---内存数据表; key---待查询记录的key; data---与key相关联的data
@输出参数:      无
@返回值:       成功:返回0; 失败:返回值为负整数
@作者:        wangkeyue
@版本:        V1.0 创建
*********************************************************************************/
int SelectDbTblByKey(const struct rte_hash *htbl, const void *key, void **data);



/***************************************************************************************************************
@函数名称:      SelectDbTblByKeyIndex
@功能描述:      根据指定key的索引,在内存数据表查询该索引对应的key值
@输入参数:      htbl---内存数据表; key_index---待查询key的索引;  key---保存查询到的key
@输出参数:      无
@返回值:       成功:返回0; 失败:返回值为负整数
@作者:        wangkeyue
@版本:        V1.0 创建
***************************************************************************************************************/
int SelectDbTblByKeyIndex(const struct rte_hash *htbl,  const int32_t key_index, void **key);



/***************************************************************************************************************
@函数名称:      SelectDbTblByKeyIndex
@功能描述:      根据指定key的索引,在内存数据表查询与key相关的记录
@输入参数:      htbl---内存数据表; key_index---待查询key的索引;  key---保存查询到的key; data---与key相关联的data
@输出参数:      无
@返回值:       成功:返回0; 失败:返回值为负整数
@作者:        wangkeyue
@版本:        V1.0 创建
***************************************************************************************************************/
#if 0
int SelectDbTblByKeyIndex(const struct rte_hash *htbl,  const int32_t key_index, void **key, void **data);
#endif



/*******************************************************************************************
@函数名称:      DeleteDbTblByKey
@功能描述:      根据指定key,在内存数据表删除与key相关的记录
@输入参数:      htbl---内存数据表; key---待删除的key
@输出参数:      无
@返回值:       成功:返回0; 失败:返回-1
@作者:        wangkeyue
@版本:        V1.0 创建
*******************************************************************************************/
int32_t DeleteDbTblByKey(const struct rte_hash *htbl, const void *key);



/*********************************************************************
@函数名称:      GetDbTblByName
@功能描述:      释放内存数据表
@输入参数:      table_name---内存数据表的名称
@输出参数:      无
@返回值:       成功:返回与table_name对应的数据表对象指针; 失败退出程序
@作者:        wangkeyue
@版本:        V1.0 创建
*********************************************************************/
const struct rte_hash *GetDbTblByName(const char *table_name);



/**********************************************************************************************************
@函数名称:      DbTblIterAll
@功能描述:      遍历数据表,打印数据表中的key-value对记录
@输入参数:      htbl---内存数据表; key---记录的key值; data---与key相关联的data; next_id---保存下一个entry的索引
@输出参数:      无
@返回值:       成功:返回0; 失败:返回值为负整数
@作者:        wangkeyue
@版本:        V1.0 创建
***************************************************************************************************************/
int32_t DbTblIterAll(const struct rte_hash *htbl, const void **key, void **data, uint32_t *next_id);



/*******************************************************************************************
@函数名称:      DestroyDbTbl
@功能描述:      释放内存数据表
@输入参数:      htbl---待释放的内存数据表
@输出参数:      无
@返回值:       无
@作者:        wangkeyue
@版本:        V1.0 创建
*******************************************************************************************/
void DestroyDbTbl(struct rte_hash *htbl);
void delDbTblRegisterArray();


#endif
