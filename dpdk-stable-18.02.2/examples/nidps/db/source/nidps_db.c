/**************************************************************************************************
*
@file name:    nidps_db.c
@description:  building memory database table based on dpdk lib_hash
@author:       wangkeyue
@time:         2021-03-16
@note:         user or caller should maintain memory management of data-record passing by paramater
@version:      V1.0
***************************************************************************************************/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <errno.h>
#include <sys/queue.h>

#include <rte_memory.h>
#include <rte_launch.h>
#include <rte_eal.h>
#include <rte_per_lcore.h>
#include <rte_lcore.h>
#include <rte_debug.h>
#include <rte_malloc.h>


#include "nidps_db.h"


#ifdef __cplusplus
extern "C" {
#endif



/**********************************全局变量申明区域**********************************/
static struct nidps_db_name_and_object *g_DbTblObjPtr[MAX_DB_TBL_NUM];





/***************************************************************************************************************************************
@函数名称:      RegisterDbTblByName
@功能描述:      通过数据表名称注册数据表类型
@输入参数:      params---创建内存数据表的参数
@输出参数:      无
@返回值:       成功:返回数据表名称的索引;   失败:返回-1
@作者:        wangkeyue
@版本:        V1.0 创建
***************************************************************************************************************************************/
static int RegisterDbTblByName(const struct rte_hash_parameters *params)
{
    //检查指定数据表名称的合法性
    if(unlikely(NULL == params || NULL == params->name || strlen(params->name) >= RTE_HASH_NAMESIZE))
    {
        RTE_LOG(ERR, HASH, "[%s][%s][line %d] table_name is NULL or table_name's length >= 32\n",__FILE__,__func__,__LINE__);
        return NIDPS_FUNC_RET_SINGED_FAILED;
    }
    //注册并保存数据表名称
    unsigned int table_name_len = strlen(params->name);
    int index = -1;
    for(index = 0; index < MAX_DB_TBL_NUM; index++)
    {
        //当前索引位置已经被注册
        if(NULL != g_DbTblObjPtr[index])
        {
            //对已经注册的名称进行匹配
            if(!strncmp(g_DbTblObjPtr[index]->dbtblname,params->name,table_name_len))
            {
                RTE_LOG(ERR, HASH, "[%s][%s][line %d] table name[%s] has been registered!\n",__FILE__,__func__,__LINE__,params->name);
                return index;
            }
        }
        else
        {
            //优先从当前程序运行lcore所在的NUMA ID上预留一块内存,不成功则尝试在其他NUMA ID上预留一块内存
            g_DbTblObjPtr[index] = rte_zmalloc_socket("struct nidps_db_name_and_object",sizeof(struct nidps_db_name_and_object),RTE_CACHE_LINE_SIZE, params->socket_id);
            if(unlikely(NULL == g_DbTblObjPtr[index]))
            {
                RTE_LOG(ERR, HASH, "[%s][%s][line %d] g_DbTblObjPtr[index] allocation failed!\n",__FILE__,__func__,__LINE__);
                return NIDPS_FUNC_RET_SINGED_FAILED;
            }

            //优先从当前程序运行lcore所在的NUMA ID上预留一块内存,不成功则尝试在其他NUMA ID上预留一块内存
            memcpy(g_DbTblObjPtr[index]->dbtblname, params->name, P2V_NAME_LEN -1);
            return index;
        }
    }
    RTE_LOG(ERR, HASH, "[%s][%s][line %d] the number of table is greater than default [%d] size!\n",__FILE__,__func__,__LINE__,MAX_DB_TBL_NUM);
    return NIDPS_FUNC_RET_SINGED_FAILED;
}



/***********************************************************************************************************************
@函数名称:      CreateDbTbl
@功能描述:      根据指定名称,创建内存数据表
@输入参数:      params---创建内存数据表的参数
@输出参数:      无
@返回值:       成功:返回数据表;   失败:退出程序
@作者:        wangkeyue
@版本:        V1.0 创建
***********************************************************************************************************************/
struct rte_hash *CreateDbTbl(const struct rte_hash_parameters *params)
{
    //根据数据表名称进行注册
    int index = RegisterDbTblByName(params);
    if(unlikely(NIDPS_FUNC_RET_SINGED_FAILED == index))
    {
        RTE_LOG(ERR, HASH, "[%s][%s][line %d] CreateDbTbl failed,tbl_name is invalid!\n",__FILE__,__func__,__LINE__);
        return NULL;
    }
    if (NULL != g_DbTblObjPtr[index]->dbtblptr)
    {
        return g_DbTblObjPtr[index]->dbtblptr;
    }
    //根据指定参数,创建哈希表
    g_DbTblObjPtr[index]->dbtblptr = rte_hash_create(params);
    if(unlikely(NULL == g_DbTblObjPtr[index]->dbtblptr))
    {
        RTE_LOG(ERR, HASH, "[%s][%s][line %d] rte_hash_create failed,tbl_name is invalid!\n",__FILE__,__func__,__LINE__);
        rte_free(g_DbTblObjPtr[index]);
        return NULL;
    }
    //注意及时释放动态申请的内存,防止内存泄漏
    return g_DbTblObjPtr[index]->dbtblptr;
}



/***********************************************************************************************************************
@函数名称:      InsertOrUpdateDbTblByKey
@功能描述:      根据指定key,向内存数据表插入data,如果key已经存在,
则更新key相关联的data
@输入参数:      htbl---内存数据表; key---待插入记录的key; data---待插入记录的data
@输出参数:      无
@返回值:       成功:返回0;   失败:返回值为负整数
@作者:        wangkeyue
@版本:        V1.0 创建
***********************************************************************************************************************/
int InsertDbTblByKey(const struct rte_hash *htbl, const void *key, void *data)
{
    if(unlikely(NULL == htbl || NULL == key || NULL == data))
    {
        RTE_LOG(ERR, HASH, "[%s][%s][line %d] input parameter htbl or key or data is NULL!\n",__FILE__,__func__,__LINE__);
        return NIDPS_FUNC_RET_SINGED_FAILED;
    }
    return rte_hash_add_key_data(htbl, key,data);
}
 
  

/***********************************************************************************************************************
@函数名称:      SelectDbTblByKey
@功能描述:      根据指定key,在内存数据表查询与key相关的记录
@输入参数:      htbl---内存数据表; key---待查询记录的key; data---与key相关联的data
@输出参数:      无
@返回值:       成功:返回key的索引;   失败:返回值为负整数
@作者:        wangkeyue
@版本:        V1.0 创建
***********************************************************************************************************************/
int SelectDbTblByKey(const struct rte_hash *htbl, const void *key, void **data)
{
    if(unlikely(NULL == htbl || NULL == key || NULL == data))
    {
        RTE_LOG(ERR, HASH, "[%s][%s][line %d] input parameter htbl or key or data is NULL!\n",__FILE__,__func__,__LINE__);
        return -ENOENT;
    }
    return rte_hash_lookup_data(htbl, key,data);
}
     


/***********************************************************************************************************************
@函数名称:      SelectDbTblByKeyIndex
@功能描述:      根据指定key的索引,在内存数据表查询该索引对应的key值
@输入参数:      htbl---内存数据表; key_index---待查询key的索引;            key---保存查询到的key
@输出参数:      无
@返回值:       成功:返回0;      失败:返回值为负整数
@作者:        wangkeyue
@版本:        V1.0 创建
***********************************************************************************************************************/
int SelectDbTblByKeyIndex(const struct rte_hash *htbl,  const int32_t key_index, void **key)
{
    if(unlikely(NULL == htbl || NULL == key || NULL == *key))
    {
        RTE_LOG(ERR, HASH, "[%s][%s][line %d] input parameter htbl or key or *key is NULL!\n",__FILE__,__func__,__LINE__);
        return NIDPS_FUNC_RET_SINGED_FAILED;
    }
    return rte_hash_get_key_with_position(htbl, key_index, key);
}



/***********************************************************************************************************************
@函数名称:      SelectDbTblByKeyIndex
@功能描述:      根据指定key的索引,在内存数据表查询与key相关的记录
@输入参数:      htbl---内存数据表; key_index---待查询key的索引;            key---保存查询到的key; data---与key相关联的data
@输出参数:      无
@返回值:       成功:返回0;      失败:返回值为负整数
@作者:        wangkeyue
@版本:        V1.0 创建
***********************************************************************************************************************/
#if 0
int SelectDbTblByKeyIndex(const struct rte_hash *htbl,  const int32_t key_index, void **key, void **data)
{
    if(unlikely(NULL == htbl || NULL == key || NULL == *key))
    {
        RTE_LOG(ERR, HASH, "[%s][%s][line %d] input parameter htbl or key or *key is NULL!\n",__FILE__,__func__,__LINE__);
        return NIDPS_FUNC_RET_SINGED_FAILED;
    }
    //返回查找的状态         0--成功; 非0--失败
    int index_to_key_ret = rte_hash_get_key_with_position(htbl, key_index, key);
    if(0 == index_to_key_ret)
    {
        return SelectDbTblByKey(htbl, *key, data);//先根据key_index索引查找对应的key值,再根据key查找关联的data
    }
    return NIDPS_FUNC_RET_SINGED_FAILED;
}
#endif



/***********************************************************************************************************************
@函数名称:      DeleteDbTblByKey
@功能描述:      根据指定key,在内存数据表删除与key相关的记录
@输入参数:      htbl---内存数据表; key---待删除的key
@输出参数:      无
@返回值:       成功:返回0;           失败:返回-1
@作者:        wangkeyue
@版本:        V1.0 创建
***********************************************************************************************************************/
int32_t DeleteDbTblByKey(const struct rte_hash *htbl, const void *key)
{
    if(unlikely(NULL == htbl || NULL == key))
    {
        RTE_LOG(ERR, HASH, "[%s][%s][line %d] input parameter htbl or key is NULL!\n",__FILE__,__func__,__LINE__);
        return NIDPS_FUNC_RET_SINGED_FAILED;
    }
    
    int32_t delete_key_index = rte_hash_del_key(htbl, key);
    //查询到待删除key的索引位置,需要调用rte_hash_free_key_with_position进行free
    if(delete_key_index < 0)
    {

        return NIDPS_FUNC_RET_SINGED_FAILED;
    }
    return NIDPS_FUNC_RET_SINGED_SUCCESS;
}



/***********************************************************************************************************************
@函数名称:      GetDbTblByName
@功能描述:      释放内存数据表
@输入参数:      table_name---内存数据表的名称
@输出参数:      无
@返回值:       成功:返回与table_name对应的数据表对象指针; 失败退出程序
@作者:        wangkeyue
@版本:        V1.0 创建
***********************************************************************************************************************/
const struct rte_hash *GetDbTblByName(const char *table_name)
{
    if(unlikely(NULL == table_name))
    {
        RTE_LOG(ERR, HASH, "[%s][%s][line %d] input parameter table_name is NULL!\n",__FILE__,__func__,__LINE__);
        rte_panic("[%s][%s][line %d] input parameter table_name is NULL!\n",__FILE__,__func__,__LINE__);
    }
    //通过数据表名称,反查对应的数据表对象指针在数组中的位置索引
    
    //注册并保存数据表名称
    int dbtbl_index = -1;
    for(dbtbl_index = 0; dbtbl_index < MAX_DB_TBL_NUM; dbtbl_index++)
    {
        //当前索引位置已经被注册
        if(NULL != g_DbTblObjPtr[dbtbl_index])
        {
            //对已经注册的名称进行匹配
            if(!strncmp(g_DbTblObjPtr[dbtbl_index]->dbtblname,table_name,strlen(table_name)))
            {
                return g_DbTblObjPtr[dbtbl_index]->dbtblptr;
            }
        }
        //到此处,意味着前面非空的所有位置都已经匹配了,后续不用再进行匹配了
        else
        {
            RTE_LOG(ERR, HASH, "[%s][%s][line %d] GetDbTblByName failed,tbl_name is invalid!\n",__FILE__,__func__,__LINE__);
            rte_panic("[%s][%s][line %d] GetDbTblByName failed,tbl_name is invalid!\n",__FILE__,__func__,__LINE__);
        }
    }
}



/***************************************************************************************************************
@函数名称:      DbTblIterAll
@功能描述:      遍历数据表,打印数据表中的key-value对记录
@输入参数:      htbl---内存数据表; key---记录的key值; data---与key相关联的data; next_id---保存下一个entry的索引
@输出参数:      无
@返回值:       成功:返回0; 失败:返回值为负整数
@作者:        wangkeyue
@版本:        V1.0 创建
***************************************************************************************************************/
int32_t DbTblIterAll(const struct rte_hash *htbl, const void **key, void **data, uint32_t *next_id)
{
    if(unlikely(NULL == htbl || NULL == next_id))
    {
        RTE_LOG(ERR, HASH, "[%s][%s][line %d] htbl or next_id is NULL!\n",__FILE__,__func__,__LINE__);
        return NIDPS_FUNC_RET_SINGED_FAILED;
    }
    while(rte_hash_iterate(htbl, key, data, next_id) >= 0) 
    {
        printf("[%s]:[%s]\n",*key,*data);
    }
}



/***********************************************************************************************************************
@函数名称:      DestroyDbTbl
@功能描述:      释放内存数据表
@输入参数:      htbl---待释放的内存数据表
@输出参数:      无
@返回值:       无
@作者:        wangkeyue
@版本:        V1.0 创建
***********************************************************************************************************************/
void DestroyDbTbl(struct rte_hash *htbl)
{
    if(unlikely(NULL == htbl))
    {
        RTE_LOG(ERR, HASH, "[%s][%s][line %d] input parameter htbl is NULL!\n",__FILE__,__func__,__LINE__);
        rte_panic("[%s][%s][line %d] input parameter htbl is NULL!\n",__FILE__,__func__,__LINE__);
    }
    rte_hash_free(htbl);
}


void delDbTblRegisterArray()
{
    int index ;
    for( index = 0; index < MAX_DB_TBL_NUM; index ++ )
    {
        if (NULL != g_DbTblObjPtr[index])
        {
            rte_free(g_DbTblObjPtr[index]);
        }
    }
    return ;
}

#ifdef __cplusplus
}

#endif

