#include "common.h"


unsigned int p2vMurMurHash(const void *key, int len, const int seed )
{
    const unsigned int m = 0x5bd1e995;
    const int r = 24;
    unsigned int h = seed ^ len;
    // Mix 4 bytes at a time into the hash
    const unsigned char *data = (const unsigned char *)key;
    while(len >= 4)
    {
        unsigned int k = *(unsigned int *)data;
        k *= m; 
        k ^= k >> r; 
        k *= m; 
        h *= m; 
        h ^= k;
        data += 4;
        len -= 4;
    }
            // Handle the last few bytes of the input array
    switch(len)
    {
        case 3: h ^= data[2] << 16;
        case 2: h ^= data[1] << 8;
        case 1: h ^= data[0];
        h *= m;
    };
    // Do a few final mixes of the hash to ensure the last few
    // bytes are well-incorporated.
    h ^= h >> 13;
    h *= m;
    h ^= h >> 15;
    return h;
}



double nidps_mean(double old_mean, uint32_t old_M, double new_val)
{
	double total_mean = (old_M * old_mean + new_val) / (old_M + 1);

	return total_mean;
}



double nidps_std(double old_std, double old_mean, uint32_t old_M, double new_val)
{
	double total_mean = nidps_mean(old_mean, old_M, new_val);
	
	double left = old_M * (pow(old_std, 2) + pow((total_mean - old_mean), 2));
	double right = pow((total_mean - new_val), 2);

	double total_std = sqrt((left + right) / (old_M + 1));

	return total_std;
}

double nidps_variance(double old_std, double old_mean, uint32_t old_M, double new_val)
{
	double total_mean = nidps_mean(old_mean, old_M, new_val);
	
	double left = old_M * (pow(old_std, 2) + pow((total_mean - old_mean), 2));
	double right = pow((total_mean - new_val), 2);

	double variance = (left + right) / (old_M + 1);

	return variance;
}


