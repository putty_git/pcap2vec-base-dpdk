/***************************************************************************************************
@file name:    protocol_common.h
@description:  common protocol struct filed definition
@author:       wangkeyue
@version:      V1.0
***************************************************************************************************/
#ifndef _PROTOCOL_COMMON_H
#define _PROTOCOL_COMMON_H


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <errno.h>
#include <ctype.h>
#include <arpa/inet.h>


typedef unsigned char uint8_t;
typedef unsigned short int uint16_t;
typedef unsigned int uint32_t;
typedef unsigned long int uint64_t;



/* 以太报文类型 */
#define ETHER_TYPE_IPV4  0x0800  /**< IPv4 Protocol. */
#define ETHER_TYPE_IPV6  0x86DD  /**< IPv6 Protocol. */
#define ETHER_TYPE_VLAN  0x8100  /**< IEEE 802.1Q VLAN tagging. */
#define ETHER_TYPE_QINQ  0x88A8  /**< IEEE 802.1ad QinQ tagging. */
#define ETHER_TYPE_QINQ1 0x9100  /**< Deprecated QinQ VLAN. */
#define ETHER_TYPE_QINQ2 0x9200  /**< Deprecated QinQ VLAN. */
#define ETHER_TYPE_QINQ3 0x9300  /**< Deprecated QinQ VLAN. */
#define ETHER_TYPE_MPLS  0x8847  /**< MPLS ethertype. */
#define ETHER_TYPE_MPLSM 0x8848  /**< MPLS multicast ethertype. */


#ifndef MAX_IPV6_EXT_HDRS
#define MAX_IPV6_EXT_HDRS  5
#endif

#ifndef IPPROTO_HOPOPTS
#define IPPROTO_HOPOPTS    0
#endif

#ifndef IPPROTO_ROUTING
#define IPPROTO_ROUTING    43
#endif

#ifndef IPPROTO_FRAGMENT
#define IPPROTO_FRAGMENT  44
#endif

#ifndef IPPROTO_NONE
#define IPPROTO_NONE      59
#endif

#ifndef IPPROTO_DSTOPTS
#define IPPROTO_DSTOPTS   60
#endif

#ifndef ETHER_ADDR_LEN
#define ETHER_ADDR_LEN   6                                       /**< Length of Ethernet address. */
#endif

#ifndef ETHER_TYPE_LEN
#define ETHER_TYPE_LEN   2                                       /**< Length of Ethernet type field. */
#endif

#ifndef ETHER_HDR_LEN
#define ETHER_HDR_LEN    (ETHER_ADDR_LEN * 2 + ETHER_TYPE_LEN)   /**< Length of Ethernet header. */
#endif

//理论上MPLS标签可以无限嵌套,此处设置解析允许的最大嵌套层数
#ifndef  MAX_MPLS_HDR_NEST_NUM
#define  MAX_MPLS_HDR_NEST_NUM   5
#endif

#ifndef IPV4
#define IPV4 4
#endif

#ifndef IPV6
#define IPV6 6
#endif

#ifndef IPPROTO_TCP
#define IPPROTO_TCP 6
#endif

#ifndef IPPROTO_UDP
#define IPPROTO_UDP 17
#endif


/* Fragment Offset Flags. */
#define	RTE_IPV4_HDR_DF_SHIFT	14
#define	RTE_IPV4_HDR_MF_SHIFT	13
#define	RTE_IPV4_HDR_FO_SHIFT	3

#define	RTE_IPV4_HDR_DF_FLAG	    (1 << RTE_IPV4_HDR_DF_SHIFT)
#define	RTE_IPV4_HDR_MF_FLAG	    (1 << RTE_IPV4_HDR_MF_SHIFT)
#define	RTE_IPV4_HDR_OFFSET_MASK	((1 << RTE_IPV4_HDR_MF_SHIFT) - 1)
#define	RTE_IPV4_HDR_OFFSET_UNITS	8


enum rte_tcp_flg
{
    TCP_URG_FLAG = 0x20,
    TCP_ACK_FLAG = 0x10,
    TCP_PSH_FLAG = 0x08,
    TCP_RST_FLAG = 0x04,
    TCP_SYN_FLAG = 0x02,
    TCP_FIN_FLAG = 0x01,
    TCP_FLAG_ALL = 0x3F,
};

enum rte_pkt_dir
{
    DIR_FWD,
    DIR_BWD,
};

enum rte_tcp_close_status
{
    FIN_WAIT_1          = 1, /*单方向fin*/
    FIN_WAIT_2          = 2, /*收到对端对第一个Fin的ack*/
    FIN_WAIT_TIME       = 3, /*发送第二个Fin*/
    TCP_CLOSE           = 4, /*收到对第二个fin的ack*/
    TCP_RST             = 5, /*tcp流收到rst报文加速删流*/
};
typedef struct ether_addr 
{
	uint8_t addr_bytes[ETHER_ADDR_LEN]; /**< Addr bytes in tx order */
}ETHER_ADDR;


/* Ethernet header: Contains the destination address, source address and frame type. */
typedef struct ether_hdr 
{
	ETHER_ADDR d_addr;         /**< Destination address. */
	ETHER_ADDR s_addr;         /**< Source address. */
	uint16_t   ether_type;     /**< Frame type. */
}ETHER_HDR;


/* Ethernet VLAN头部 */
typedef struct vlan_hdr 
{
	uint16_t vlan_tci;    /**< Priority (3) + CFI (1) + Identifier Code (12) */
	uint16_t eth_proto;   /**< Ethernet type of encapsulated frame. */
}VLAN_HDR;


/* MPLS头部 */
typedef struct mpls_hdr
{
	uint16_t tag_msb;    /**< Label(msb). */
	uint8_t  tag_lsb:4;  /**< Label(lsb). */
	uint8_t  tc:3;       /**< Traffic class. */
	uint8_t  bs:1;       /**< Bottom of stack. */
	uint8_t  ttl;        /**< Time to live. */
}MPLS_HDR;


/* IPv4头部 */
typedef struct ipv4_hdr 
{
	uint8_t   version_ihl;		/**< version and header length */
	uint8_t   type_of_service;	/**< type of service */
	uint16_t  total_length;	    /**< length of packet */
	uint16_t  packet_id;		/**< packet ID */
	uint16_t  fragment_offset;	/**< fragmentation offset */
	uint8_t   time_to_live;		/**< time to live */
	uint8_t   next_proto_id;    /**< protocol ID */
	uint16_t  hdr_checksum;	    /**< header checksum */
	uint32_t  src_addr;		    /**< source address */
	uint32_t  dst_addr;		    /**< destination address */
}IPV4_HDR;


/* IPv6头部 */
typedef struct ipv6_hdr
{
	uint32_t vtc_flow;	        /**< IP version, traffic class & flow label. */
	uint16_t payload_len;	    /**< IP packet length - includes header size */
	uint8_t  proto;		        /**< Protocol, next header. */
	uint8_t  hop_limits;	    /**< Hop limits. */
	uint8_t  src_addr[16];	    /**< IP address of source host. */
	uint8_t  dst_addr[16];	    /**< IP address of destination host(s). */
} IPV6_HDR;


/* TCP头部字段 */
typedef struct tcp_hdr 
{
	uint16_t src_port;          /**< TCP source port. */
	uint16_t dst_port;          /**< TCP destination port. */
	uint32_t sent_seq;          /**< TX data sequence number. */
	uint32_t recv_ack;          /**< RX data acknowledgment sequence number. */
	uint8_t  data_off;          /**< Data offset. */
	uint8_t  tcp_flags;         /**< TCP flags */
	uint16_t rx_win;            /**< RX flow control window. */
	uint16_t cksum;             /**< TCP checksum. */
	uint16_t tcp_urp;           /**< TCP urgent pointer, if any. */
}TCP_HDR;


/* UDP头部字段 */
typedef struct udp_hdr 
{
	uint16_t src_port;    /**< UDP source port. */
	uint16_t dst_port;    /**< UDP destination port. */
	uint16_t dgram_len;   /**< UDP datagram length */
	uint16_t dgram_cksum; /**< UDP datagram checksum */
}UDP_HDR;


typedef struct  ipv4_2tuple
{
    uint32_t  srcip_v4;
    uint32_t  dstip_v4;
}IPV4_2TUPLE;


typedef struct  ipv6_2tuple
{
    uint8_t  srcip_v6[16];
    uint8_t  dstip_v6[16];
}IPV6_2TUPLE;


//5元组结构
typedef struct  five_tuple_key
{
    //ip-2元组结构
    union ip_2tuple
    {
        IPV4_2TUPLE  ipv4_2tuple;
        IPV6_2TUPLE  ipv6_2tuple;
    }ip2tuple;
    uint16_t    src_port;       //源端口
    uint16_t    dst_port;       //目的端口
    uint8_t     ipproto_type;   //6.tcp  17.udp
}FIVE_TUPLE_KEY;


//单条数据包统计字段
typedef struct pkt_stats_info
{
    uint8_t  pkt_ip_version;          //数据包IP类型(4.IPV4;      6.IPV6)
    uint8_t  pkt_ipproto_id;          //数据包传输层承载的协议类型(6.tcp;              17.udp)
    uint16_t pkt_len;  	              //数据包长度(bytes)
    uint16_t pkt_mac_hdr_len;         //数据包L2层头部长度(bytes)
    uint16_t pkt_ip_hdr_len;          //数据包L3层头部长度(bytes)
    
    uint16_t pkt_trans_hdr_len;       //数据包L4层头部长度(bytes)
	uint16_t tcp_rx_window;           //滑动窗口
	uint32_t tcp_sent_seq;            //序号
	
	uint32_t tcp_recv_ack;            //确认序号
	uint8_t  tcp_flags;               //TCP flags标志
	uint8_t  pkt_dir;                  /*当前报文方向*/
    uint16_t  ip_len;               /*ip总长度*/
    double    pkt_timestamp;          //数据包时间戳信息(s)
}PKT_STATS_INFO;


//单条连接的统计字段
typedef struct link_stats_info
{
    uint8_t   ipproto_id;             //连接类型(6.tcp;     17.udp)
    
    double     fwd_start_time;         //连接fwd方向建立的开始时间
    double     fwd_finish_time;        //连接fwd方向结束的开始时间
    double     bwd_start_time;         //连接bwd方向建立的开始时间
    double     bwd_finish_time;        //连接bwd方向结束的开始时间
    double     flow_duration_time;     //流持续时间
    
    uint32_t  total_fwd_pkt;  	      //连接fwd方向总的报文个数
    uint32_t  total_fwd_bytes;        //连接fwd方向总的报文长度(bytes)
    uint32_t  total_bwd_pkt;         //连接bwd方向总的报文个数
    uint32_t  total_bwd_bytes;        //连接bwd方向总的报文长度(bytes)
	
    uint32_t  min_fwd_bytes;           //连接fwd方向报文的最小长度(bytes)
    uint32_t  max_fwd_bytes;           //连接fwd方向报文的最大长度(bytes)
    uint32_t  min_bwd_bytes;           //连接bwd方向报文的最小长度(bytes)
    uint32_t  max_bwd_bytes;           //连接bwd方向报文的最大长度(bytes)
	double    flow_bytes_second;       //每秒流字节数
	double    flow_pkts_second;        //每秒流报文数
	double    fwd_pkts_second;         //每秒上行报文数
	double    bwd_pkts_second;         //每秒下行报文数

	
	uint32_t  min_pkts_len;            //报文最小长度(bytes)
	uint32_t  max_pkts_len;            //报文最大长度(bytes)
	double    mean_pkts_len;           //报文平均长度(bytes)
	double    std_pkts_len;            //报文长度标准差
	double    variance_pkts_len;       //报文长度方差
	double    down_up_ratio;           //下行报文个数/上行报文个数
	double    average_packet_size;     //报文平均大小(bytes)
	
	double    mean_fwd_bytes;          //连接fwd方向报文的平均长度(bytes)
	double    std_fwd_bytes;           //连接fwd方向报文的长度标准差(bytes)
	double    mean_bwd_bytes;          //连接bwd方向报文的平均长度(bytes)
	double    std_bwd_bytes;           //连接bwd方向报文的长度标准差(bytes)
	
    /** gy 负责更新     start */
    uint32_t  fwd_push_flag_num;       //连接fwd方向报文中push标志位生效的次数(UDP设置为0)
    uint32_t  fwd_urg_flag_num;    	   //连接fwd方向报文中urg标志位生效的次数(UDP设置为0)
    uint32_t  bwd_push_flag_num;       //连接bwd方向报文中push标志位生效的次数(UDP设置为0)
    uint32_t  bwd_urg_flag_num;        //连接bwd方向报文中urg标志位生效的次数(UDP设置为0)
    
    uint32_t  fwd_hdr_total_bytes;     //连接fwd方向报文的头部长度总和
    uint32_t  bwd_hdr_total_bytes;     //连接bwd方向报文的头部长度总和

	/** modify by gy */
    uint32_t  cwr_flag_pkts;           //cwr flag标志位生效的报文个数
    /** modify by gy */
    uint32_t  ece_flag_pkts;           //ece flag标志位生效的报文个数
    uint32_t  urg_flag_pkts;           //urg flag标志位生效的报文个数
	uint32_t  psh_flag_pkts;           //psh flag标志位生效的报文个数
    uint32_t  ack_flag_pkts;           //ack flag标志位生效的报文个数
    uint32_t  rst_flag_pkts;           //rst flag标志位生效的报文个数
    uint32_t  syn_flag_pkts;           //syn flag标志位生效的报文个数
    uint32_t  fin_flag_pkts;           //fin flag标志位生效的报文个数
    
    /** gy incre */
	double fwd_segment_size_avg; 	/** Average tcp payload size observed in the forward direction */
	double bwd_segment_size_avg; 	/** Average tcp payloadsize observed in the backward direction */
	
	uint32_t total_fwd_tcp_pkts;  	//连接fwd方向总的tcp报文个数
    uint32_t total_bwd_tcp_pkts;    //连接bwd方向总的tcp报文个数
    uint32_t total_bwd_pkts;        //连接bwd方向总的报文个数
	uint32_t total_fwd_pkts;		//连接fwd方向总的报文个数
		
	double fwd_iat_min; 			/** Minimum time between two packets sent in the forward direction */
	double fwd_iat_max; 			/** Maximum time between two packets sent in the forward direction */
	double fwd_iat_mean; 		/** Mean time between two packets sent in the forward direction */
	double fwd_iat_std; 			/** Standard deviation time between two packets sent in the forward direction */
	double fwd_iat_total; 		/** Total time between two packets sent in the forward direction */
	
	double bwd_iat_min; 			/** Minimum time between two packets sent in the backward direction */
	double bwd_iat_max;			/** Maximum time between two packets sent in the backward direction */
	double bwd_iat_mean; 		/** Mean time between two packets sent in the backward direction */
	double bwd_iat_std; 			/** Standard deviation time between two packets sent in the backward direction */
	double bwd_iat_total; 		/** Total time between two packets sent in the backward direction */

	double last_fwd_pkt_arrive_time;  /** when fwd pkt is first comming, last_fwd_pkt_arrive_time is negative */
	double last_bwd_pkt_arrive_time;  /** when bwd pkt is first comming, last_bwd_pkt_arrive_time is negative */
	double last_pkt_arrive_time;  	 /** when pkt is first comming, last_pkt_arrive_time is negative */

	double flow_iat_mean; 		/** Mean time between two packets sent in the flow */
	double flow_iat_std; 		/** Standard deviation time between two packets sent in the flow */
	double flow_iat_max; 		/** Maximum time between two packets sent in the flow */
	double flow_iat_min; 		/** Minimum time between two packets sent in the flow */

	uint16_t fwd_tcp_rx_first_window;     	   /** tcp上行报文第一个窗口 */
	uint16_t bwd_tcp_rx_first_window;     	   /** tcp下行报文第一个窗口 */
	uint16_t fwd_tcp_first_flg;
	uint16_t bwd_tcp_first_flg;
	
	uint32_t fwd_init_winbytes;				   /** 窗口切换之前，上行报文字节数总数 */
	uint32_t bwd_init_winbytes;

	uint32_t fwd_act_data_pkts;                /** 上行带载荷报文个数        */
	uint32_t fwd_seg_size_min;			       /** 最小tcp传输层长度        */

	uint32_t active_and_idle_init_flg;
	uint32_t idle_iat_count;
	double idle_iat_timestamp_start;            /* 每个空口期的起始时间 */
	double idle_iat_timestamp_end;            /* 每个空口期的起始时间 */
	uint32_t lastpkt_payload_zeor_flag;
	uint32_t last_idle_iat_duration;
	uint32_t syn_flg;
	uint32_t synack_flg;
	uint32_t fin_or_rst_flg;


	double idle_min;						      /* 空口期，双方互发ack保活 */
	double idle_mean;
	double idle_max;
	double idle_std;

	uint32_t active_iat_count;
	double active_iat_timestamp_start;            /* 每个活跃期的起始时间 */
	double active_iat_timestamp_end;
	
	uint32_t active_first_flg;
	uint32_t active_no_need_cal_flg;

	double active_min;						  
	double active_mean;
	double active_max;
	double active_std;

    /** gy 负责更新     end */

}LINK_STATS_INFO;


#endif
