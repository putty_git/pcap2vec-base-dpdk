#ifndef _COMMON_H_
#define _COMMON_H_

#include <math.h>
#include <stdint.h>


/** float equal */
#define FLOAT_EPS 	1e-6

#define nidps_min(x, y)   (((x) < (y)) ? (x) : (y))

#define nidps_max(x, y)   (((x) > (y)) ? (x) : (y))


/** \brief  增量计算海量数据均值、方差、标准差
 *			https://blog.csdn.net/ssmixi/article/details/104927111
 *
 *  \param  old_std  	: 旧样本的标准差
 *			old_mean 	: 旧样本的平均值
 * 			old_M	 	: 旧样本的数量
 *			new_val     : 新增的一个样本值
 *
 *	\retval total_mean	: 新旧样本的平均值	
 *			total_std	: 新旧样本的标准差
 */

double nidps_mean(double old_mean, uint32_t old_M, double new_val);

double nidps_std(double old_std, double old_mean, uint32_t old_M, double new_val);

double nidps_variance(double old_std, double old_mean, uint32_t old_M, double new_val);

unsigned int p2vMurMurHash(const void *key, int len ,const int seed);

#endif