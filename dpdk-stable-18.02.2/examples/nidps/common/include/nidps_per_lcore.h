#ifndef _NIDPS_PER_LCORE_H_
#define _NIDPS_PER_LCORE_H_

#include  "rte_per_lcore.h"

typedef struct rte_threadvar_desc
{
    struct rte_hash         *ptFlowTbl;
    p2v_pcapfile_mng        *ptPcapFileMng;
    p2v_pktmbuf_mp          *ptPktMbufMp;
    p2v_output_eigenval_buf  *ptOutputEigenvalBuf;
    char*                    ptInPcapName;
    char*                    ptFlwTblName;
    unsigned int             flowTblAgeIndex; /*线程流表老化*/
    u32                      flowTblAgeTime;
    u32                      flowTblSz;
    u32                      flowRdCrtCount;
    u32                      flwNrmDelCount;
    u32                      flowRdDelCount;
    u32                      procPktsCount;
    u32                      parsePktsErrCount;
    u32                      flwTblCrtFailPkts;
}p2v_threadvar_desc;

/**
 * Macro to define a per lcore variable "var" of type "type", don't
 * use keywords like "static" or "volatile" in type, just prefix the
 * whole macro.
 */



/**
 * Macro to declare an extern per lcore variable "var" of type "type"
 */

RTE_DECLARE_PER_LCORE( p2v_threadvar_desc , p2vThrVarDes);
/**
 * Read/write the per-lcore variable value
 */

#define P2V_GET_THRVAEDES_LCORE_PARA() RTE_PER_LCORE(p2vThrVarDes)






#endif
