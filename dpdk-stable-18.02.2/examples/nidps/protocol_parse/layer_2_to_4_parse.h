/***************************************************************************************************
@file name:    layer_2_to_4_parse.h
@description:  parse basic protocol header from pkt
@author:       wangkeyue
@version:      V1.0
***************************************************************************************************/
#ifndef _LAYER_2_TO_4_PARSE_H
#define _LAYER_2_TO_4_PARSE_H


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <ctype.h>
#include <arpa/inet.h>
#include <pcap.h>
#include <unistd.h>
#include <fcntl.h>

#include "protocol_common.h"



/*******************************************************************************************************
@函数名称:      ParseVlanHeadLen
@功能描述:      通过解析数据包,获取Vlan头部的长度
@输入参数:      pkt---数据包指针; pktlen---数据包长度; offset---数据包相对于起始位置的偏移长度
@输出参数:      ether_type---保存IPV4/6版本信息
@返回值:       成功:返回Vlan头部的长度; 失败:返回负数
@作者:        wangkeyue
@版本:        V1.0 创建
*******************************************************************************************************/
int ParseVlanHeadLen(const uint8_t *pkt, uint16_t pktlen, uint16_t offset, uint16_t *ether_type);



/******************************************************************************************************
@函数名称:      ParseQinqHeadLen
@功能描述:      通过解析数据包,获取Qinq头部的长度,qinq头部是在vlan头部又封装了一层
@输入参数:      pkt---数据包指针; pktlen---数据包长度; offset---数据包相对于起始位置的偏移长度
@输出参数:      ether_type---保存IPV4/6版本信息
@返回值:       成功:返回Qinq头部的长度; 失败:返回负数
@作者:        wangkeyue
@版本:        V1.0 创建
*******************************************************************************************************/
int ParseQinqHeadLen(const uint8_t *pkt, uint16_t pktlen, uint16_t offset, uint16_t *ether_type);



/******************************************************************************************************
@函数名称:      ParseMplsHeadLen
@功能描述:      通过解析数据包,获取Mpls头部的长度
@输入参数:      pkt---数据包指针; pktlen---数据包长度; offset---数据包相对于起始位置的偏移长度
@输出参数:      ether_type---保存IPV4/6版本信息
@返回值:       成功:返回Mpls头部的长度; 失败:返回负数
@作者:        wangkeyue
@版本:        V1.0 创建
*******************************************************************************************************/
int ParseMplsHeadLen(const uint8_t *pkt, uint16_t pktlen, uint16_t offset, uint16_t *ether_type);



/**************************************************************************************************************
@函数名称:      ParseIpV6ExtendHdr
@功能描述:      解析IPV6扩展头部字段,返回最内层的proto
@输入参数:      pkt---数据包指针; pktlen---数据包长度; proto---数据包协议类型; offset---偏移长度
@输出参数:      frag---保存是否为分片的标志
@返回值:       成功:返回proto; 失败:返回-1
@作者:        wangkeyue
@版本:        V1.0 创建
***************************************************************************************************************/
int ParseIpV6ExtendHdr(const uint8_t *pkt, uint16_t pktlen, uint16_t proto, uint32_t *offset, int *frag);



/******************************************************************************************************************************************************************************
@函数名称:      ParseL2ToL4LayerProtoInfo
@功能描述:      通过解析数据包,解析L2-L4层的协议字段信息
@输入参数:      pkt---数据包指针
			pkt_buffer_len---存储数据包缓存的实际大小
			pkthdr---数据包头部指针
@输出参数:      pkt_stats_info---保存数据包解析统计信息
			tuple_info---保存数据包5元组信息
@返回值:       成功:0,   失败:返回负数
@作者:        wangkeyue
@版本:        V1.0 创建
******************************************************************************************************************************************************************************/
int ParseL2ToL4LayerProtoInfo(const uint8_t *pkt, const struct pcap_pkthdr *pkthdr, FIVE_TUPLE_KEY *tuple_info, PKT_STATS_INFO *pkt_stats_info, uint16_t pkt_buffer_len);



#endif
