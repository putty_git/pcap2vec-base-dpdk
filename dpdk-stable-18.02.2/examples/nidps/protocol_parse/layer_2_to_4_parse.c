/***************************************************************************************************
@file name:    layer_2_to_4_parse.c
@description:  parse basic protocol header from pkt
@author:       wangkeyue
@version:      V1.0
***************************************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif


#include "layer_2_to_4_parse.h"


/************************************************************************************************************************
@函数名称:      ParseVlanHeadLen
@功能描述:      通过解析数据包,获取Vlan头部的长度
@输入参数:      pkt---数据包指针; pktlen---数据包长度; offset---数据包相对于起始位置的偏移长度
@输出参数:      ether_type---保存IPV4/6版本信息
@返回值:       成功:返回Vlan头部的长度; 失败:返回负数
@作者:        wangkeyue
@版本:        V1.0 创建
************************************************************************************************************************/
int ParseVlanHeadLen(const uint8_t *pkt, uint16_t pktlen, uint16_t offset, uint16_t *ether_type)
{
    //输入参数合法性检查
    if(NULL == pkt || NULL == ether_type)
    {
        printf("[%s][%s][line %d] pkt or ether_type is NULL!\n",__FILE__,__func__,__LINE__);
        return -1;
    }
    if(offset + sizeof(VLAN_HDR) > pktlen)
    {
        printf("[%s][%s][line %d] vlan pkt length is invalid!\n",__FILE__,__func__,__LINE__);
        return (-1 * 2);
    }
    
    VLAN_HDR *vlan_head = (VLAN_HDR *)(pkt + offset);
    //判断底层承载的协议是否为IP协议
    *ether_type = ntohs(vlan_head->eth_proto);
    if(*ether_type == ETHER_TYPE_IPV4 || *ether_type == ETHER_TYPE_IPV6)
    {
        return sizeof(VLAN_HDR);
    }
    else
    {
        printf("[%s][%s][line %d] not ipv4 or ipv6 pkt in low-inner-layer!\n",__FILE__,__func__,__LINE__);
        return (-1 * 3);;
    }
}



/****************************************************************************************************************************************
@函数名称:      ParseQinqHeadLen
@功能描述:      通过解析数据包,获取Qinq头部的长度,qinq头部是在vlan头部又封装了一层
@输入参数:      pkt---数据包指针; pktlen---数据包长度; offset---数据包相对于起始位置的偏移长度
@输出参数:      ether_type---保存IPV4/6版本信息
@返回值:       成功:返回Qinq头部的长度; 失败:返回负数
@作者:        wangkeyue
@版本:        V1.0 创建
****************************************************************************************************************************************/
int ParseQinqHeadLen(const uint8_t *pkt, uint16_t pktlen, uint16_t offset, uint16_t *ether_type)
{
    //输入参数合法性检查
    if(NULL == pkt || NULL == ether_type)
    {
        printf("[%s][%s][line %d] pkt or ether_type is NULL!\n",__FILE__,__func__,__LINE__);
        return -1;
    }
    if(offset + 2 * sizeof(VLAN_HDR) > pktlen)
    {
        printf("[%s][%s][line %d] QinQ pkt length is invalid!\n",__FILE__,__func__,__LINE__);
        return (-1 * 2);
    }

    VLAN_HDR *qinq_head_outer = NULL;
    qinq_head_outer = (VLAN_HDR *)(pkt + offset);
    //内层的vlan头部协议字段必须为0x8100
    uint16_t inner_vlan_type = ntohs(qinq_head_outer->eth_proto);
    if(inner_vlan_type != ETHER_TYPE_VLAN)
    {
        printf("[%s][%s][line %d] not qinq pkt,because inner vlan type is not 0x8100!\n",__FILE__,__func__,__LINE__);
        return (-1 * 3);
    }
    
    //内层承载的协议是否为IP协议的判断    
    VLAN_HDR  *qinq_head_inner = (VLAN_HDR *)(qinq_head_outer + 1);
    *ether_type = ntohs(qinq_head_inner->eth_proto);
    if(*ether_type == ETHER_TYPE_IPV4 || *ether_type == ETHER_TYPE_IPV6)
    {
        return (2 * sizeof(VLAN_HDR)); //qinq的报文头部是固定2层vlan头部的长度
    }
    else
    {
        printf("[%s][%s][line %d] qinq:inner-vlan: not ipv4 or ipv6 pkt in low-inner-layer!\n",__FILE__,__func__,__LINE__);
        return (-1 * 4);
    } 
}



/**************************************************************************************************************************************************
@函数名称:      ParseMplsHeadLen
@功能描述:      通过解析数据包,获取Mpls头部的长度
@输入参数:      pkt---数据包指针; pktlen---数据包长度; offset---数据包相对于起始位置的偏移长度
@输出参数:      ether_type---保存IPV4/6版本信息
@返回值:       成功:返回Mpls头部的长度; 失败:返回负数
@作者:        wangkeyue
@版本:        V1.0 创建
**************************************************************************************************************************************************/
int ParseMplsHeadLen(const uint8_t *pkt, uint16_t pktlen, uint16_t offset, uint16_t *ether_type)
{
    //输入参数合法性检查    
    if(NULL == pkt || NULL == ether_type)
    {
        printf("[%s][%s][line %d] rte_mbuf or ether_type is NULL!\n",__FILE__,__func__,__LINE__);
        return -1;
    }
    
    uint8_t i = 0;
    uint8_t bottom_label_stack_flag = 0;
    uint16_t mpls_head_len_single = sizeof(MPLS_HDR);
    MPLS_HDR *mpls_head = NULL;
    
    //解析嵌套的MPLS头部
    for(i = 0; i < MAX_MPLS_HDR_NEST_NUM; i++)
    {
        //根据指定的偏移,从mbuf中读取数据包相应的内容
        if(offset + (i * mpls_head_len_single) > pktlen)
        {
            printf("[%s][%s][line %d] mpls pkt length is invalid!\n",__FILE__,__func__,__LINE__);
            return (-1 * 2);
        }
        mpls_head = (MPLS_HDR *)(pkt + offset + (i * mpls_head_len_single));
        const char *mpls_head_next = (const char *)mpls_head;
        //获取嵌套标志位信息
        bottom_label_stack_flag = (*(mpls_head_next + 2)) & 0x01;
        //MPLS标签嵌套标志位,1代表为最内层嵌套,0代表还有后续嵌套
        if(1 == bottom_label_stack_flag)
        {
            //读取IP头部的verion字段
            uint8_t ip_verion = ((*(mpls_head_next + mpls_head_len_single)) & 0xf0) >> 4;        
            if(IPV4 == ip_verion)
            {
                *ether_type = ETHER_TYPE_IPV4;
                return (mpls_head_len_single * i);  //MPLS嵌套头部的总长度信息
            }
            else if(IPV6 == ip_verion)
            {
                *ether_type = ETHER_TYPE_IPV6;
                return (mpls_head_len_single * i);  //MPLS嵌套头部的总长度信息
            }
            else
            {
               printf("[%s][%s][line %d] not ipv4 or ipv6 pkt in low-inner-layer!\n",__FILE__,__func__,__LINE__);
                return (-1 * 3);
            }
        }        
     }
     
     printf("[%s][%s][line %d] mpls nest layer exceeds %d!\n",__FILE__,__func__,__LINE__,MAX_MPLS_HDR_NEST_NUM);
     return (-1 * 4);
}



/***********************************************************************************************************
@函数名称:      ParseIpV6ExtendHdr
@功能描述:      解析IPV6扩展头部字段,返回最内层的proto
@输入参数:      pkt---数据包指针; pktlen---数据包长度; proto---数据包协议类型; offset---偏移长度
@输出参数:      frag---保存是否为分片的标志
@返回值:       成功:返回proto; 失败:返回-1
@作者:        wangkeyue
@版本:        V1.0 创建
************************************************************************************************************/
int ParseIpV6ExtendHdr(const uint8_t *pkt, uint16_t pktlen, uint16_t proto, uint32_t *offset, int *frag)
{
	struct ext_hdr 
	{
		uint8_t next_hdr;
		uint8_t len;
	};
	const struct ext_hdr *xh;
	unsigned int i;

	*frag = 0;
	for(i = 0; i < MAX_IPV6_EXT_HDRS; i++) 
	{
		switch(proto) 
		{
    		case IPPROTO_HOPOPTS:
    		case IPPROTO_ROUTING:
    		case IPPROTO_DSTOPTS:
    		{
    		    if(*offset + sizeof(struct ext_hdr) > pktlen)
    			{
    			    return -1;
    			}
    			xh = (const struct ext_hdr *)(pkt + *offset + sizeof(struct ext_hdr));
    			*offset += (xh->len + 1) * 8;
    			proto = xh->next_hdr;
    			break;
    	    }
    		case IPPROTO_FRAGMENT:
    		{
                if(*offset + sizeof(struct ext_hdr) > pktlen)
                {
                    return -1;
                }
                xh = (const struct ext_hdr *)(pkt + *offset + sizeof(struct ext_hdr));
    			*offset += 8;
    			proto = xh->next_hdr;
    			*frag = 1;
    			return proto; //最后最外层一个扩展头部的协议类型
    	    }
    		case IPPROTO_NONE:
    		{
    		    return 0;
    		}
    		default:
    		{
    			return proto;
    	    }
		}
	}
	
	return -1;
}



/******************************************************************************************************************************************************************************
@函数名称:		ParseL2ToL4LayerProtoInfo
@功能描述:		通过解析数据包,解析L2-L4层的协议字段信息
@输入参数:		pkt---数据包指针
			pkt_buffer_len---存储数据包缓存的实际大小
			pkthdr---数据包头部指针
@输出参数:		pkt_stats_info---保存数据包解析统计信息
			tuple_info---保存数据包5元组信息
@返回值: 	  成功:0,   失败:返回负数
@作者:		wangkeyue
@版本:		V1.0 创建
******************************************************************************************************************************************************************************/
int ParseL2ToL4LayerProtoInfo(const uint8_t *pkt, const struct pcap_pkthdr *pkthdr, FIVE_TUPLE_KEY *tuple_info, PKT_STATS_INFO *pkt_stats_info, uint16_t pkt_buffer_len)
{
	//输入参数合法性检查
	if(NULL == pkt || NULL == pkthdr || ETHER_HDR_LEN >= pkthdr->caplen || NULL == pkt_stats_info || NULL == tuple_info)
	{
		printf("[%s][%s][line %d] pkt or pkthdr or pkt_stats_info or tuple_info is NULL or pktlen is invalid!\n",__FILE__,__func__,__LINE__);
		return -1;
	}
	uint16_t pktlen = 0;
	//libpcap捕获了超长数据包,修正长度为buffer实际缓存的数据包长
	if(pkt_buffer_len < pkthdr->caplen)
	{
		pktlen = pkt_buffer_len;
	}
	else if(pkt_buffer_len == pkthdr->caplen)
	{
		pktlen = pkthdr->caplen;
	}
	//获取数据包的时间戳(单位为秒(double类型))
	double timestamp_second = pkthdr->ts.tv_sec + ((double)(pkthdr->ts.tv_usec)) / (1000 * 1000);
	
	uint16_t ether_proto_type = 0;	  //保存ether_type字段信息
	uint32_t off_from_start   = 0;	  //数据包相对于pkt起始位置的偏移长度
	int parse_offset_head_len = 0;	  //保存需要偏移的长度信息
	int last_ext_hdr		  = -1;   //判断是否是IPV6最后一个扩展头部
	int frag_flag			  = 0;	  //分片标志, 0.非分片,		  1.分片
	
	ETHER_HDR *eth_head  = NULL;
	IPV4_HDR  *ipv4_head = NULL;
	IPV6_HDR  *ipv6_head = NULL;
	TCP_HDR   *tcp_head  = NULL;
	UDP_HDR   *udp_head  = NULL;
	
	
	/********************************************开始-解析MAC头部字段*******************************************/
	//读取MAC头部内容
	eth_head = (ETHER_HDR *)pkt;
	ether_proto_type = ntohs(eth_head->ether_type); //从原始报文buf中获取ether_type字段信息
	off_from_start	 = sizeof(ETHER_HDR);			//L2层数据包头部长度偏移
	
	switch(ether_proto_type)
	{
		//IPV4
		case ETHER_TYPE_IPV4:
		{
			break;
		}
		//IPV6
		case ETHER_TYPE_IPV6:
		{
			break;
		}
		//VLAN
		case ETHER_TYPE_VLAN:
		{
			//获取VLAN头部长度的偏移信息
			parse_offset_head_len = ParseVlanHeadLen(pkt, pktlen, off_from_start, &ether_proto_type);
			break;
		}
		//MPLS
		case ETHER_TYPE_MPLS:
		case ETHER_TYPE_MPLSM:
		{
			//获取MPLS头部长度的偏移信息
			parse_offset_head_len = ParseMplsHeadLen(pkt, pktlen, off_from_start, &ether_proto_type);
			break;
		}
		//QINQ
		case ETHER_TYPE_QINQ:
		case ETHER_TYPE_QINQ1:
		case ETHER_TYPE_QINQ2:
		case ETHER_TYPE_QINQ3:
		{
			//获取QINQ头部长度的偏移信息
			parse_offset_head_len = ParseQinqHeadLen(pkt, pktlen, off_from_start, &ether_proto_type);
			break;
		}
		default:
		{
			printf("[%s][%s][line %d] ether_proto_type is not support.\n",__FILE__,__func__,__LINE__);
			return (-1 * 2);
		}
	}
	//判断解析2.5层头部的状态(仅支持ipv4/v6)
	if(parse_offset_head_len < 0)
	{
		printf("[%s][%s][line %d] 2.5-layer HeaderLen is invalid!\n",__FILE__,__func__,__LINE__);
		return (-1 * 3);
	}
	//根据解析头部长度,计算需要剥离2层头部的总长度
	pkt_stats_info->pkt_len = pkthdr->caplen;  //捕获到的原始数据包长度
	pkt_stats_info->pkt_timestamp = timestamp_second;
	off_from_start += parse_offset_head_len;
	pkt_stats_info->pkt_mac_hdr_len = off_from_start;
	/********************************************结束-解析MAC头部字段*******************************************/


	switch(ether_proto_type)
	{
		case ETHER_TYPE_IPV4:
		{
			//读取IPV4头部内容
			if(off_from_start + sizeof(IPV4_HDR) > pktlen)
			{
				printf("[%s][%s][line %d] ipv4 pkt length is invalid!\n",__FILE__,__func__,__LINE__);
				return (-1 * 4);
			}
			
			ipv4_head = (IPV4_HDR *)(pkt + off_from_start);
			uint16_t flag_offset	   = ntohs(ipv4_head->fragment_offset);
			uint16_t ip_frag_offset    = (uint16_t)(flag_offset & RTE_IPV4_HDR_OFFSET_MASK);
			uint16_t ip_more_frag_flag = (uint16_t)(flag_offset & RTE_IPV4_HDR_MF_FLAG);
			
			//IPV4分片包,忽略
			if(ip_frag_offset != 0 || ip_more_frag_flag != 0  )
			{
				printf("[%s][%s][line %d] not support ipv4-fragment pkt!\n",__FILE__,__func__,__LINE__);
				return (-1 * 5);
			}
			
			pkt_stats_info->pkt_ip_version = IPV4;
			uint8_t ipv4_hdr_len = (uint8_t)((ipv4_head->version_ihl & 0x0f) * 4);
			pkt_stats_info->pkt_ip_hdr_len = ipv4_hdr_len;
			off_from_start += ipv4_hdr_len;
			pkt_stats_info->pkt_ipproto_id = ipv4_head->next_proto_id;

			//校验数据包长度与协议字段取值是否合法
			if(ipv4_hdr_len > (pktlen  - pkt_stats_info->pkt_mac_hdr_len))
			{
				printf("[%s][%s][line %d] ipv4 pkt length invalid!\n",__FILE__,__func__,__LINE__);
				return (-1 * 6);
			}
			uint16_t ip_total_len = ntohs(ipv4_head->total_length);
			pkt_stats_info->ip_len	= ip_total_len;
			
			//提取srcip、dstip二元组信息
			tuple_info->ip2tuple.ipv4_2tuple.srcip_v4 = ntohl(ipv4_head->src_addr);
			tuple_info->ip2tuple.ipv4_2tuple.dstip_v4 = ntohl(ipv4_head->dst_addr);

			if(pkt_stats_info->pkt_ipproto_id == IPPROTO_TCP)
			{
				//读取TCP头部内容
				if(off_from_start + sizeof(TCP_HDR) > pktlen)
				{
					printf("%s][%s][line %d] ipv4 tcp pkt length is invalid!\n",__FILE__,__func__,__LINE__);
					return (-1 * 7);
				}
				tcp_head = (TCP_HDR *)(pkt + off_from_start);
				uint16_t currtcphdrlen = ((tcp_head->data_off & 0xf0) >> 4 ) * 4;//提取TCP的头部长度信息
				pkt_stats_info->pkt_trans_hdr_len = currtcphdrlen;

				//提取PORT + IPPROTO
				tuple_info->src_port	 = ntohs(tcp_head->src_port);
				tuple_info->dst_port	 = ntohs(tcp_head->dst_port);
				tuple_info->ipproto_type = IPPROTO_TCP;

				//TCP头部统计相关的字段赋值
				pkt_stats_info->tcp_sent_seq  = ntohl(tcp_head->sent_seq);
				pkt_stats_info->tcp_recv_ack  = ntohl(tcp_head->recv_ack);
				pkt_stats_info->tcp_flags	  = tcp_head->tcp_flags;
				pkt_stats_info->tcp_rx_window = ntohs(tcp_head->rx_win);
			}
			else if(pkt_stats_info->pkt_ipproto_id == IPPROTO_UDP)
			{
				if(off_from_start + sizeof(UDP_HDR) > pktlen)
				{
					printf("%s][%s][line %d] ipv4 udp pkt length is invalid!\n",__FILE__,__func__,__LINE__);
					return (-1 * 8);
				}
				udp_head = (UDP_HDR *)(pkt + off_from_start);
				uint16_t udp_total_len = ntohs(udp_head->dgram_len);
				if(udp_total_len > (ip_total_len - pkt_stats_info->pkt_ip_hdr_len))
				{
					printf("%s][%s][line %d] ipv4 udp pkt length is error!\n",__FILE__,__func__,__LINE__);
					return (-1 * 9);
				}
				uint16_t currudphdrlen = sizeof(UDP_HDR);
				pkt_stats_info->pkt_trans_hdr_len = currudphdrlen;
				
				//提取PORT + IPPROTO
				tuple_info->src_port	 = ntohs(udp_head->src_port);
				tuple_info->dst_port	 = ntohs(udp_head->dst_port);
				tuple_info->ipproto_type = IPPROTO_UDP;
			}
			
			break;
		}
		case ETHER_TYPE_IPV6:
		{
			//读取IPV6头部内容
			if(off_from_start + sizeof(IPV6_HDR) > pktlen)
			{
				printf("[%s][%s][line %d] ipv6 pkt length is invalid!\n",__FILE__,__func__,__LINE__);
				return (-1 * 10);
			}

			pkt_stats_info->pkt_ip_version = IPV6;
			uint32_t off_from_start_tmp = off_from_start;
			ipv6_head = (IPV6_HDR *)(pkt + off_from_start);
			
			//ipv6头部为首部固定长度40字节,需要继续解析内层可能的扩展头部
			uint16_t ipv6_fixed_hdr_len = sizeof(IPV6_HDR);
			//继续偏移IPV6头部固定的长度至扩展头部开始的位置
			off_from_start += ipv6_fixed_hdr_len;
			//解析IPV6的扩展头部
			last_ext_hdr = ParseIpV6ExtendHdr(pkt, pktlen, ipv6_head->proto,  &off_from_start, &frag_flag);
			if(last_ext_hdr < 0)
			{
				printf("[%s][%s][line %d] ipv6 pkt extend-header format error!\n",__FILE__,__func__,__LINE__);
				return (-1 * 11);
			}
			//IPV6分片包,忽略
			if(frag_flag == 1)
			{
				printf("[%s][%s][line %d] not support ipv6-fragment pkt!\n",__FILE__,__func__,__LINE__);
				return (-1 * 12);
			}

			pkt_stats_info->ip_len	= ipv6_fixed_hdr_len + ntohs(ipv6_head->payload_len);
			pkt_stats_info->pkt_ip_hdr_len = (off_from_start - off_from_start_tmp);
			pkt_stats_info->pkt_ipproto_id = last_ext_hdr;

			if(pkt_stats_info->pkt_ip_hdr_len > (pktlen - pkt_stats_info->pkt_mac_hdr_len))
			{
				printf("[%s][%s][line %d] ipv6 header legth invalid!\n",__FILE__,__func__,__LINE__);
				return (-1 * 13);
			}

			//提取srcip、dstip二元组
			memcpy(tuple_info->ip2tuple.ipv6_2tuple.srcip_v6, ipv6_head->src_addr, 16);
			memcpy(tuple_info->ip2tuple.ipv6_2tuple.dstip_v6, ipv6_head->dst_addr, 16);

			if(pkt_stats_info->pkt_ipproto_id == IPPROTO_TCP)
			{
				//读取TCP头部内容
				if(off_from_start + sizeof(TCP_HDR) > pktlen)
				{
					printf("%s][%s][line %d] ipv6 tcp pkt length is invalid!\n",__FILE__,__func__,__LINE__);
					return (-1 * 14);
				}
				tcp_head = (TCP_HDR *)(pkt + off_from_start);
				uint16_t currtcphdrlen = ((tcp_head->data_off & 0xf0) >> 4 ) * 4;//提取TCP的头部长度信息
				pkt_stats_info->pkt_trans_hdr_len = currtcphdrlen;

				//提取PORtuple_infoT + IPPROTO
				tuple_info->src_port	 = ntohs(tcp_head->src_port);
				tuple_info->dst_port	 = ntohs(tcp_head->dst_port);
				tuple_info->ipproto_type = IPPROTO_TCP;

				//TCP头部统计相关的字段赋值
				pkt_stats_info->tcp_sent_seq  = ntohl(tcp_head->sent_seq);
				pkt_stats_info->tcp_recv_ack  = ntohl(tcp_head->recv_ack);
				pkt_stats_info->tcp_flags	  = tcp_head->tcp_flags;
				pkt_stats_info->tcp_rx_window = ntohs(tcp_head->rx_win);
			}
			else if(pkt_stats_info->pkt_ipproto_id == IPPROTO_UDP)
			{
				if(off_from_start + sizeof(UDP_HDR) > pktlen)
				{
					printf("%s][%s][line %d] ipv6 udp pkt length is invalid!\n",__FILE__,__func__,__LINE__);
					return (-1 * 15);
				}
				udp_head = (UDP_HDR *)(pkt + off_from_start);
				uint16_t currudphdrlen = sizeof(UDP_HDR);
				pkt_stats_info->pkt_trans_hdr_len = currudphdrlen;
				
				//提取PORT + IPPROTO
				tuple_info->src_port	 = ntohs(udp_head->src_port);
				tuple_info->dst_port	 = ntohs(udp_head->dst_port);
				tuple_info->ipproto_type = IPPROTO_UDP;
			}

			break;
		}
		default:
		{
			printf("[%s][%s][line %d] not ip type pkt, not support!\n",__FILE__,__func__,__LINE__);
			return (-1 * 16);
		}
	}
	
	return 0;

}



#ifdef __cplusplus
}
#endif
